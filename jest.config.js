module.exports = {
  testMatch: [
    '<rootDir>/test/**/*.(?:spec|test).ts'
  ],
  collectCoverage: false,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.ts'
  ],
  coverageDirectory: '<rootDir>/coverage',
  coverageReporters: [
    'text-summary',
    'html'
  ],
  globalSetup: '<rootDir>/test/jest.setup.ts',
  preset: 'ts-jest',
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      packageJson: 'package.json',
      tsConfig: 'test/tsconfig.json',
    },
  },
};
