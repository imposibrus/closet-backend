
declare module 'admin-panel' {
    import {RequestHandler} from 'express';

    interface Options {
        express: any; // express object
        models: any; // models definitions
        adminConfig: any;
        storagePath?: string;
        customControlsDir?: string;
        loginCallback: RequestHandler;
    }

    function s(options: Options): any;

    namespace s {}

    export = s;
}
