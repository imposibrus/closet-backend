
interface IntelInterface extends IntelLoggerInstance {
    basicConfig(options: any): IntelLoggerInstance;
    getLogger(loggerName?: string): IntelLoggerInstance;
}

interface IntelLoggerInstance {
    critical(...args: any[]): any;
    debug(...args: any[]): any;
}

declare module 'intel' {
    export = intel;
}

declare var intel: IntelInterface;
