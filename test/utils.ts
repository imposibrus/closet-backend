import * as path from 'path';
import * as models from '../src/models';
import pathsHelper from '../src/pathsHelper';
import {get} from 'lodash';

export const assertAdvert = (advert: any) => {
    expect(advert).toMatchObject({
        title: 'Диван',
        text: 'Красивый',
        coordinates: defaultCoordinates,
        questionsCount: 0,
    });
    expect(advert).toEqual(expect.objectContaining({
        id: expect.any(Number),
        images: expect.any(Array),
        User: expect.any(Object),
        Category: expect.any(Object),
    }));

    expect(advert.images).toHaveLength(1);
    expect(advert.images[0]).toMatchObject({
        url: 'http://127.0.0.1/public/storage/path.jpg',
        previews: {
            '150x150': 'http://127.0.0.1/public/storage/path_preview.jpg',
        },
    });
    expect(advert.images[0]).toHaveProperty('id');

    assertUser(advert.User);

    expect(advert.Category).toMatchObject({
        title: 'Сантехника',
        costFrom: 1000,
        costUntil: 5000,
        betsActive: true,
    });
    expect(advert.Category).toHaveProperty('id');
};

export const assertUser = (user: any) => {
    expect(user).toMatchObject({
        firstName: 'firstName',
        lastName: 'lastName',
        city: 'Kazan',
    });
    expect(user).toEqual(expect.objectContaining({
        id: expect.any(Number),
        phone: expect.any(String),
    }));
};

export const createAdvert = async () => {
    return models.Advert.create({
        title: 'Диван',
        text: 'Красивый',
        coordinates: {
            type: 'Point',
            coordinates: defaultCoordinates.slice().reverse(),
        },
        status: 'active',
        Media: {
            path: path.join(pathsHelper.storage, 'path.jpg'),
            meta: {
                previews: {
                    '150x150': {
                        path: path.join(pathsHelper.storage, 'path_preview.jpg'),
                    },
                },
            },
        },
        User: {
            firstName: 'firstName',
            lastName: 'lastName',
            phone: await incrementalUserPhone(),
            city: 'Kazan',
            password: '123',
            activated: true,
        },
        Category: {
            title: 'Сантехника',
            costFrom: 1000,
            costUntil: 5000,
        },
    }, {include: [models.Media, {model: models.User, as: 'User'}, models.Category]});
};

export const incrementalUserPhone = async () => {
    return Number(
        get<models.User, 'phone', number>(
            await models.User.findOne({attributes: ['phone'], order: [['phone', 'DESC']], paranoid: false}),
            'phone',
            79297285597
        )
    ) + 1;
};

export const defaultCoordinates = [12.345, 67.890];

export const defaultGeoQuery = {lat: defaultCoordinates[0], lon: defaultCoordinates[1], radius: 20000};
