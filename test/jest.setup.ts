import * as models from '../src/models';

module.exports = () => {
    return models.sequelize.sync({force: true});
};
