import * as path from 'path';
import * as request from 'supertest';
import redis from '../../src/lib/redisSharedConnection';
import * as models from '../../src/models';
import server from '../../src/bin/www';
import {assertAdvert, assertUser, createAdvert, defaultCoordinates, defaultGeoQuery} from '../utils';

describe('Ads', () => {
    let createdUser: models.User;

    beforeAll(async () => {
        // FIXME: instead of force `sync`ing here, try to wrap each test in transaction
        await models.sequelize.sync({force: true});
        const createdAd = await createAdvert();

        if (!createdAd.User) {
            throw new Error('`createdAd.User` is empty');
        }

        createdUser = createdAd.User;
    });

    test('should retrieve ads list', () => {
        return request(server)
            .get('/api/ads')
            .query(defaultGeoQuery)
            .expect('Content-Type', /application\/json/)
            .expect(200)
            .then((res) => {
                expect(res.body.status).toBe(200);
                expect(res.body.ads).toHaveLength(1);

                assertAdvert(res.body.ads[0]);
            });
    });

    test('should retrieve ads list within one category', async () => {
        const foundCategory = await models.Category.findOne({where: {title: 'Сантехника'}});

        if (!foundCategory) {
            throw new Error('`foundCategory` is empty');
        }

        return request(server)
            .get(`/api/categories/${foundCategory.id}/ads`)
            .expect('Content-Type', /application\/json/)
            .expect(200)
            .then((res) => {
                expect(res.body.status).toBe(200);
                expect(res.body.ads).toHaveLength(1);

                assertAdvert(res.body.ads[0]);
            });
    });

    test('should retrieve one ad by id', async () => {
        const foundAdvert = await models.Advert.findOne({where: {title: 'Диван'}});

        if (!foundAdvert) {
            throw new Error('`foundAdvert` is empty');
        }

        return request(server)
            .get(`/api/ads/${foundAdvert.id}`)
            .expect('Content-Type', /application\/json/)
            .expect(200)
            .then((res) => {
                expect(res.body.status).toBe(200);

                assertAdvert(res.body.ad);
            });
    });

    describe('My Ads', () => {
        beforeAll(() => {
            return redis.set('closet-backend:tokens:token_here', createdUser.id);
        });
        afterAll(() => {
            return redis.del('closet-backend:tokens:token_here');
        });

        test('should retrieve user own ads', () => {
            return request(server)
                .get(`/api/ads/my?token=token_here`)
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                    expect(res.body.ads).toHaveLength(1);

                    assertAdvert(res.body.ads[0]);
                });
        });
    });

    describe('Creating ads', () => {
        let newMedia: models.Media, newCategory: models.Category;

        beforeAll(async () => {
            await redis.set('closet-backend:tokens:token_here', createdUser.id);

            newMedia = await models.Media.create({
                path: path.join(__dirname, '..', '..', 'public/storage/path.jpg'),
                meta: {
                    previews: {
                        '150x150': {
                            path: path.join(__dirname, '..', '..', 'public/storage/path_preview.jpg'),
                        },
                    },
                },
            });
            newCategory = await models.Category.create({
                title: 'Сантехника',
                costFrom: 1000,
                costUntil: 5000,
            });
        });
        afterAll(() => {
            return redis.del('closet-backend:tokens:token_here');
        });

        test('should create ad without media and categories', () => {
            return request(server)
                .post(`/api/ads?token=token_here`)
                .send({
                    title: 'title',
                    text: 'text',
                    coordinates: defaultCoordinates,
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);

                    expect(res.body.ad).toEqual(expect.objectContaining({
                        id: expect.any(Number),
                        User: expect.any(Object),
                        Category: expect.any(Object),
                    }));

                    expect(res.body.ad).toMatchObject({
                        title: 'title',
                        text: 'text',
                        coordinates: defaultCoordinates,
                        images: [],
                        questionsCount: 0,
                    });

                    assertUser(res.body.ad.User);
                });
        });

        test('should create ad with media and categories', () => {
            return request(server)
                .post(`/api/ads?token=token_here`)
                .send({
                    title: 'Диван',
                    text: 'Красивый',
                    coordinates: defaultCoordinates,
                    imagesIds: [newMedia.id],
                    categoryId: newCategory.id,
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);

                    assertAdvert(res.body.ad);
                });
        });
    });

    describe('Updating ads', () => {
        let createdAd: models.Advert, newMedia: models.Media, newCategory: models.Category;

        beforeAll(async () => {
            await redis.set('closet-backend:tokens:token_here', createdUser.id);

            newMedia = await models.Media.create({
                path: path.join(__dirname, '..', '..', 'public/storage/path.jpg'),
                meta: {
                    previews: {
                        '150x150': {
                            path: path.join(__dirname, '..', '..', 'public/storage/path_preview.jpg'),
                        },
                    },
                },
            });
            newCategory = await models.Category.create({
                title: 'Сантехника',
                costFrom: 1000,
                costUntil: 5000,
            });

            createdAd = await models.Advert.create({
                title: 'title',
                text: 'text',
                coordinates: {type: 'Point', coordinates: defaultCoordinates.slice().reverse()},
                mediaIds: [newMedia.id],
                CategoryId: newCategory.id,
                UserId: createdUser.id,
                status: 'active',
            });
        });
        afterAll(() => {
            return redis.del('closet-backend:tokens:token_here');
        });

        test('should update ad and remove media and categories from it', () => {
            return request(server)
                .put(`/api/ads/${createdAd.id}?token=token_here`)
                .send({
                    title: 'title changed',
                    text: 'text changed',
                    coordinates: [14.321, 20.876],
                    imagesIds: [],
                    categoryId: null,
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);

                    expect(res.body.ad).toEqual(expect.objectContaining({
                        id: expect.any(Number),
                        User: expect.any(Object),
                        Category: expect.any(Object),
                    }));

                    expect(res.body.ad).toMatchObject({
                        title: 'title changed',
                        text: 'text changed',
                        coordinates: [14.321, 20.876],
                        images: [],
                        questionsCount: 0,
                    });

                    assertUser(res.body.ad.User);

                    expect(res.body.ad.Category).toEqual(null);
                });
        });

        test('should update fail because of invalid status', () => {
            return request(server)
                .put(`/api/ads/${createdAd.id}?token=token_here`)
                .send({
                    title: 'title',
                    text: 'text',
                    coordinates: defaultCoordinates,
                    imagesIds: [],
                    categoryId: null,
                    status: 'overdue',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.message).toBe('status can be updated only to `paused`');
                });
        });

        test('should update advert status', () => {
            return request(server)
                .put(`/api/ads/${createdAd.id}?token=token_here`)
                .send({
                    title: 'title',
                    text: 'text',
                    coordinates: defaultCoordinates,
                    imagesIds: [],
                    categoryId: null,
                    status: 'paused',
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);

                    expect(res.body.ad).toEqual(expect.objectContaining({
                        id: expect.any(Number),
                        User: expect.any(Object),
                        Category: expect.any(Object),
                    }));

                    expect(res.body.ad).toMatchObject({
                        title: 'title',
                        text: 'text',
                        coordinates: defaultCoordinates,
                        images: [],
                        questionsCount: 0,
                    });

                    assertUser(res.body.ad.User);

                    expect(res.body.ad.Category).toEqual(null);
                });
        });
    });
});
