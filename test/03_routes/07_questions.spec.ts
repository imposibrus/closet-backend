import * as request from 'supertest';
import redis from '../../src/lib/redisSharedConnection';
import * as models from '../../src/models';
import server from '../../src/bin/www';
import {assertUser, createAdvert, defaultCoordinates, incrementalUserPhone} from '../utils';

describe('Questions', () => {
    let createdUser: models.User, createdUser2: models.User, createdMedia: models.Media[],
        createdCategory: models.Category, createdAd: models.Advert, inactiveAd: models.Advert,
        createdQuestion: models.Question;

    beforeAll(async () => {
        createdAd = await createAdvert();

        if (!createdAd.Media || !createdAd.User || !createdAd.Category) {
            throw new Error('`createdAd.Media || createdAd.User || createdAd.Category` is empty');
        }

        createdMedia = createdAd.Media;
        createdUser = createdAd.User;
        createdCategory = createdAd.Category;

        inactiveAd = await models.Advert.create({
            title: 'Диван',
            text: 'Красивый',
            coordinates: {
                type: 'Point',
                coordinates: defaultCoordinates.slice().reverse(),
            },
            status: 'overdue',
            mediaIds: createdMedia.map(m => m.id),
            UserId: createdUser.id,
            CategoryId: createdCategory.id,
        });

        createdQuestion = await createdAd.$create('Question', {questionText: 'existing question text', UserId: createdUser.id}) as models.Question;

        createdUser2 = await models.User.create({
            firstName: 'firstName',
            lastName: 'lastName',
            phone: await incrementalUserPhone(),
            city: 'Kazan',
            password: '123',
            activated: true,
        });

        await Promise.all([
            redis.set('closet-backend:tokens:token_here', createdUser.id),
            redis.set('closet-backend:tokens:token2_here', createdUser2.id),
        ]);
    });
    afterAll(() => {
        return Promise.all([
            redis.del('closet-backend:tokens:token_here'),
            redis.del('closet-backend:tokens:token2_here'),
        ]);
    });

    describe('Adding', () => {
        test('should fail and return "text required"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/questions?token=token_here`)
                .send({
                    questionText: '',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('questionText_required');
                });
        });

        test('should fail and return "no such advert"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id + 100500}/questions?token=token_here`)
                .send({
                    questionText: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('no_such_advert');
                });
        });

        test('should fail and return "no such advert"', () => {
            return request(server)
                .post(`/api/ads/${inactiveAd.id}/questions?token=token_here`)
                .send({
                    questionText: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('no_such_advert');
                });
        });

        test('should fail and return "question already asked"', () => {
            return request(server)
                .post(`/api/ads/${inactiveAd.id}/questions?token=token_here`)
                .send({
                    questionText: 'existing question text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('question_already_asked');
                });
        });

        test('should add question to advert', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/questions?token=token_here`)
                .send({
                    questionText: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                });
        });
    });

    describe('Receiving', () => {
        test('should fail and return "no such advert"', () => {
            return request(server)
                .get(`/api/ads/${createdAd.id + 100500}/questions`)
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('no_such_advert');
                });
        });

        test('should fail and return "no such advert"', () => {
            return request(server)
                .get(`/api/ads/${inactiveAd.id}/questions`)
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('no_such_advert');
                });
        });

        test('should receive questions list', () => {
            return request(server)
                .get(`/api/ads/${createdAd.id}/questions`)
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                    expect(res.body.questions).toHaveLength(2);

                    expect(res.body.questions[0]).toEqual(expect.objectContaining({
                        id: expect.any(Number),
                        date: expect.any(Number),
                        User: expect.any(Object),
                    }));
                    expect(res.body.questions[0]).toMatchObject({
                        questionText: 'some text',
                        answerText: null,
                    });
                    assertUser(res.body.questions[0].User);
                });
        });
    });

    describe('Answering', () => {
        test('should fail and return "text required"', () => {
            return request(server)
                .post(`/api/questions/${createdQuestion.id}/answer?token=token_here`)
                .send({
                    answerText: '',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('answerText_is_required');
                });
        });

        test('should fail and return "no such question"', () => {
            return request(server)
                .post(`/api/questions/${createdQuestion.id + 100500}/answer?token=token_here`)
                .send({
                    answerText: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('question_not_found');
                });
        });

        test('should fail and return "not your advert"', () => {
            return request(server)
                .post(`/api/questions/${createdQuestion.id}/answer?token=token2_here`)
                .send({
                    answerText: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('not_your_advert');
                });
        });

        test('should add answer to question', () => {
            return request(server)
                .post(`/api/questions/${createdQuestion.id}/answer?token=token_here`)
                .send({
                    answerText: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);

                    expect(res.body.question).toEqual(expect.objectContaining({
                        id: expect.any(Number),
                        date: expect.any(Number),
                        User: expect.any(Object),
                    }));
                    expect(res.body.question).toMatchObject({
                        questionText: 'existing question text',
                        answerText: 'some text',
                    });
                    assertUser(res.body.question.User);
                });
        });
    });
});
