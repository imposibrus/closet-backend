import * as request from 'supertest';
import server from '../../src/bin/www';
import * as models from '../../src/models';
import * as smsLib from '../../src/lib/SMSLib';
import * as tokenLib from '../../src/lib/tokenLib';
import redis from '../../src/lib/redisSharedConnection';
import {incrementalUserPhone} from '../utils';

describe('Register/Login', () => {
    let smsSendSpy: jest.SpyInstance, generateSmsStub: jest.SpyInstance, generateTokenStub: jest.SpyInstance,
        successUserPhone: number;

    beforeEach(() => {
        smsSendSpy = jest.spyOn(smsLib, 'send');
        generateSmsStub = jest.spyOn(smsLib, 'generate').mockImplementation(() => {
            return 1234;
        });
        generateTokenStub = jest.spyOn(tokenLib, 'generate').mockImplementation(() => {
            return 'token_here';
        });
    });

    afterEach(() => {
        smsSendSpy.mockRestore();
        generateSmsStub.mockRestore();
        generateTokenStub.mockRestore();
    });

    describe('Registration', () => {
        test('should create new user and send SMS-code', async () => {
            return request(server)
                .post('/api/registration')
                .send({ phone: `+${successUserPhone = await incrementalUserPhone()}`, password: '123qwe', firstName: 'Vadim', lastName: 'Petrov', city: 'Kazan' })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(smsLib.send).toBeCalledTimes(1);
                    expect(res.body.status).toBe(200);
                    expect(res.body.desc).toBe('user_registered_code_sent');
                });
        });

        test('shouldn\'t create new user if phone is invalid', () => {
            return request(server)
                .post('/api/registration')
                .send({ phone: '+123', password: '123qwe', firstName: 'Vadim', lastName: 'Petrov', city: 'Kazan' })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('phone_invalid');
                });
        });

        test('shouldn\'t create new user if password is empty', () => {
            return request(server)
                .post('/api/registration')
                .send({ phone: `+${successUserPhone}`, password: '', firstName: 'Vadim', lastName: 'Petrov', city: 'Kazan' })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('password_invalid');
                });
        });

        test('shouldn\'t create 2 users with same phone', () => {
            return request(server)
                .post('/api/registration')
                .send({ phone: `+${successUserPhone}`, password: '123qwe', firstName: 'Vadim', lastName: 'Petrov', city: 'Kazan' })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('phone_duplicate');
                });
        });
    });

    describe('Code confirmation', () => {
        test('should activate user and generate token', () => {
            return request(server)
                .post('/api/check_code')
                .send({ phone: `+${successUserPhone}`, code: 1234 })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                });
        });

        test('shouldn\'t activate already activated user', () => {
            return request(server)
                .post('/api/check_code')
                .send({ phone: `+${successUserPhone}`, code: 1234 })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('user_activated');
                });
        });

        test('should check phone before activating', () => {
            return request(server)
                .post('/api/check_code')
                .send({ phone: '+123', code: 1234 })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('phone_invalid');
                });
        });

        test('shouldn\'t activate non existing user', async () => {
            return request(server)
                .post('/api/check_code')
                .send({ phone: `+${await incrementalUserPhone()}`, code: 1235 })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('invalid_code_or_phone');
                });
        });
    });

    describe('Login', () => {
        let redisSetStub: jest.SpyInstance;

        beforeAll(() => {
            redisSetStub = jest.spyOn(redis, 'set').mockImplementation(() => {
                return new Promise((resolve) => {
                    process.nextTick(resolve);
                });
            });
        });
        afterAll(() => {
            redisSetStub.mockRestore();
        });

        test('should login as existing user', () => {
            return request(server)
                .post('/api/login')
                .send({ phone: `+${successUserPhone}`, password: '123qwe' })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                    expect(res.body.token).toBe('token_here');
                });
        });

        test('should check phone', () => {
            return request(server)
                .post('/api/login')
                .send({ phone: '+123', password: '123qwe' })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('phone_invalid');
                });
        });

        test('shouldn\'t login with empty password', () => {
            return request(server)
                .post('/api/login')
                .send({ phone: `+${successUserPhone}`, password: '' })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('password_invalid');
                });
        });

        test('shouldn\'t login non existing user', async () => {
            return request(server)
                .post('/api/login')
                .send({ phone: `+${await incrementalUserPhone()}`, password: '123qwe' })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('invalid_password_or_phone');
                });
        });

        describe('deactivated user', () => {
            let tmpUserId: number, tmpUserPhone: number;

            beforeAll(async () => {
                tmpUserPhone = await incrementalUserPhone();

                return models.User.create({
                    phone: tmpUserPhone,
                    password: '123qwe'
                }).then((createdUser: models.User) => {
                    tmpUserId = createdUser.id;
                });
            });

            afterAll(() => {
                return models.User.destroy({
                    where: {
                        id: tmpUserId
                    }
                });
            });

            test('shouldn\'t login deactivated user', () => {
                return request(server)
                    .post('/api/login')
                    .send({ phone: `+${tmpUserPhone}`, password: '123qwe' })
                    .expect('Content-Type', /application\/json/)
                    .expect(400)
                    .then((res) => {
                        expect(res.body.status).toBe(400);
                        expect(res.body.desc).toBe('user_deactivated');
                    });
            });
        });
    });
});
