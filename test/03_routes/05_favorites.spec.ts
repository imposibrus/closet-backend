import * as request from 'supertest';
import redis from '../../src/lib/redisSharedConnection';
import * as models from '../../src/models';
import server from '../../src/bin/www';
import {assertAdvert, createAdvert, defaultGeoQuery} from '../utils';

describe('Favorites', () => {
    let createdUser: models.User, createdAd: models.Advert;

    beforeAll(async () => {
        createdAd = await createAdvert();

        if (!createdAd.User) {
            throw new Error('`createdAd.User` is empty');
        }

        createdUser = createdAd.User;

        await redis.set('closet-backend:tokens:token_here', createdUser.id);
    });
    afterAll(() => {
        return redis.del('closet-backend:tokens:token_here');
    });

    describe('Adding', () => {
        test('should add advert to favorites list', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/favorite?token=token_here`)
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                });
        });
    });

    describe('Retrieving', () => {
        test('should get favorites list', () => {
            return request(server)
                .get(`/api/favorites?token=token_here`)
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    if (!createdAd.Media) {
                        throw new Error('`createdAd.Media` is empty');
                    }

                    expect(res.body.status).toBe(200);

                    expect(res.body.favoriteAds).toHaveLength(1);

                    assertAdvert(res.body.favoriteAds[0]);
                });
        });

        test('should retrieve ads list with marked favorites', () => {
            return request(server)
                .get('/api/ads?token=token_here')
                .query(defaultGeoQuery)
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                    const favoriteAds = res.body.ads.filter((ad: models.Advert) => ad.isFavorite);

                    expect(favoriteAds).toHaveLength(1);
                });
        });
    });

    describe('Deleting', () => {
        test('should delete one ad from favorites list', () => {
            return request(server)
                .delete(`/api/favorites/${createdAd.id}?token=token_here`)
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                });
        });
    });
});
