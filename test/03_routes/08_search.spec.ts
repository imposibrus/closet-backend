import * as path from 'path';
import * as request from 'supertest';
import redis from '../../src/lib/redisSharedConnection';
import * as models from '../../src/models';
import server from '../../src/bin/www';
import {defaultCoordinates, defaultGeoQuery, incrementalUserPhone} from '../utils';

describe('Search', () => {
    let createdUser: models.User, createdMedia: models.Media[],
        createdCategory: models.Category, createdAd: models.Advert, createdAd2: models.Advert, inactiveAd: models.Advert;

    beforeAll(async () => {
        createdAd = await models.Advert.create({
            title: 'уникальное имя',
            text: 'неповторимое описание',
            coordinates: {
                type: 'Point',
                coordinates: defaultCoordinates.slice().reverse(),
            },
            status: 'active',
            Media: {
                path: path.join(__dirname, '..', '..', 'public/storage/path.jpg'),
                meta: {
                    previews: {
                        '150x150': {
                            path: path.join(__dirname, '..', '..', 'public/storage/path_preview.jpg'),
                        },
                    },
                },
            },
            User: {
                firstName: 'firstName',
                lastName: 'lastName',
                phone: await incrementalUserPhone(),
                city: 'Kazan',
                password: '123',
                activated: true,
            },
            Category: {
                title: 'Сантехника',
                costFrom: 1000,
                costUntil: 5000,
            },
        }, {include: [models.Media, {model: models.User, as: 'User'}, models.Category]});

        if (!createdAd.Media || !createdAd.User || !createdAd.Category) {
            throw new Error('`createdAd.Media || createdAd.User || createdAd.Category` is empty');
        }

        createdMedia = createdAd.Media;
        createdUser = createdAd.User;
        createdCategory = createdAd.Category;

        inactiveAd = await models.Advert.create({
            title: 'Диван',
            text: 'Красивый',
            coordinates: {
                type: 'Point',
                coordinates: defaultCoordinates.slice().reverse(),
            },
            status: 'overdue',
            mediaIds: createdMedia.map(m => m.id),
            UserId: createdUser.id,
            CategoryId: createdCategory.id,
        });

        createdAd2 = await models.Advert.create({
            title: 'удочка',
            text: 'морская',
            coordinates: {
                type: 'Point',
                coordinates: defaultCoordinates.slice().reverse(),
            },
            status: 'active',
            mediaIds: createdMedia.map(m => m.id),
            UserId: createdUser.id,
            CategoryId: createdCategory.id,
        });

        await redis.set('closet-backend:tokens:token_here', createdUser.id);
    });
    afterAll(() => {
        return redis.del('closet-backend:tokens:token_here');
    });

    test('should find 1 result', () => {
        return request(server)
            .get(`/api/search`)
            .query({
                s: 'уникаль',
                ...defaultGeoQuery,
            })
            .expect('Content-Type', /application\/json/)
            .expect(200)
            .then((res) => {
                expect(res.body.status).toBe(200);
                expect(res.body.ads).toHaveLength(1);
            });
    });

    test('should support multiple words', () => {
        return request(server)
            .get(`/api/search`)
            .query({
                s: 'имя удочк',
                ...defaultGeoQuery,
            })
            .expect('Content-Type', /application\/json/)
            .expect(200)
            .then((res) => {
                expect(res.body.status).toBe(200);
                expect(res.body.ads).toHaveLength(2);
            });
    });
});
