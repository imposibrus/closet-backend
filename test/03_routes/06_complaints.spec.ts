import * as request from 'supertest';
import redis from '../../src/lib/redisSharedConnection';
import * as models from '../../src/models';
import server from '../../src/bin/www';
import {createAdvert} from '../utils';

describe('Complaints', () => {
    let createdUser: models.User, createdAd: models.Advert;

    beforeAll(async () => {
        createdAd = await createAdvert();

        if (!createdAd.User) {
            throw new Error('`createdAd.User` is empty');
        }

        createdUser = createdAd.User;

        await redis.set('closet-backend:tokens:token_here', createdUser.id);
    });
    afterAll(() => {
        return redis.del('closet-backend:tokens:token_here');
    });

    describe('Adding', () => {
        test('should fail and return "text required"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/complaint?token=token_here`)
                .send({
                    type: 'cant_call',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('text_required');
                });
        });

        test('should fail and return "type required"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/complaint?token=token_here`)
                .send({
                    text: 'some text',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('type_required');
                });
        });

        test('should fail and return "invalid type"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/complaint?token=token_here`)
                .send({
                    text: 'some text',
                    type: 'unknown type',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('type_invalid');
                });
        });

        test('should fail and return "no such advert"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id + 100500}/complaint?token=token_here`)
                .send({
                    text: 'some text',
                    type: 'cant_call',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('no_such_advert');
                });
        });

        test('should add complaint to advert', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/complaint?token=token_here`)
                .send({
                    text: 'some text',
                    type: 'cant_call',
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                });
        });
    });
});
