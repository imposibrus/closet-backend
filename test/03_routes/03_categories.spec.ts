import * as request from 'supertest';
import * as models from '../../src/models';
import server from '../../src/bin/www';

describe('Categories', () => {
    const categoriesCollection = [
        {
            title: 'Бытовая техника',
            costFrom: 1000,
            costUntil: 5000,
        },
        {
            title: 'Электроника',
            costFrom: 1000,
            costUntil: 5000,
        },
    ];

    beforeAll(async () => {
        // FIXME: instead of force `sync`ing here, try to wrap each test in transaction
        await models.Category.sync({force: true});

        for (const category of categoriesCollection) {
            await models.Category.create(category);
        }
    });

    test('should retrieve list of categories', () => {
        return request(server)
            .get('/api/categories')
            .expect('Content-Type', /application\/json/)
            .expect(200)
            .then((res) => {
                expect(res.body.status).toBe(200);
                expect(res.body.categories).toHaveLength(2);

                for (let i = 0; i < res.body.categories.length; i++) {
                    let category = res.body.categories[i];

                    expect(category).toEqual(expect.objectContaining({
                        id: expect.any(Number),
                        title: expect.any(String),
                        costFrom: expect.any(Number),
                        costUntil: expect.any(Number),
                        betsActive: expect.any(Boolean),
                    }));
                }
            });
    });
});
