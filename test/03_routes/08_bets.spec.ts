import * as request from 'supertest';
import redis from '../../src/lib/redisSharedConnection';
import * as models from '../../src/models';
import server from '../../src/bin/www';
import {assertAdvert, assertUser, createAdvert, defaultCoordinates, incrementalUserPhone} from '../utils';

describe('Bets', () => {
    let createdUser: models.User, createdUser2: models.User, createdMedia: models.Media[],
        createdCategory: models.Category, createdAd: models.Advert, createdAd2: models.Advert, inactiveAd: models.Advert;

    beforeAll(async () => {
        try {
            createdAd = await createAdvert();
        } catch (err) {
            console.error(err);
            throw err;
        }

        if (!createdAd.Media || !createdAd.User || !createdAd.Category) {
            throw new Error('`createdAd.Media || createdAd.User || createdAd.Category` is empty');
        }

        createdMedia = createdAd.Media;
        createdUser = createdAd.User;
        createdCategory = createdAd.Category;

        inactiveAd = await models.Advert.create({
            title: 'Диван',
            text: 'Красивый',
            coordinates: {
                type: 'Point',
                coordinates: defaultCoordinates.slice().reverse(),
            },
            status: 'overdue',
            mediaIds: createdMedia.map(m => m.id),
            UserId: createdUser.id,
            CategoryId: createdCategory.id,
        });

        createdAd2 = await models.Advert.create({
            title: 'Диван',
            text: 'Красивый',
            coordinates: {
                type: 'Point',
                coordinates: defaultCoordinates.slice().reverse(),
            },
            status: 'active',
            mediaIds: createdMedia.map(m => m.id),
            UserId: createdUser.id,
            Category: {
                title: 'Сантехника',
                costFrom: 1000,
                costUntil: 5000,
                betsActive: false,
            },
        }, {include: [models.Category]});

        createdUser2 = await models.User.create({
            firstName: 'firstName',
            lastName: 'lastName',
            phone: await incrementalUserPhone(),
            city: 'Kazan',
            password: '123',
            activated: true,
        });

        await Promise.all([
            redis.set('closet-backend:tokens:token_here', createdUser.id),
            redis.set('closet-backend:tokens:token2_here', createdUser2.id),
        ]);
    });
    afterAll(() => {
        return Promise.all([
            redis.del('closet-backend:tokens:token_here'),
            redis.del('closet-backend:tokens:token2_here'),
        ]);
    });

    describe('Adding', () => {
        test('should fail and return "cost required"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/bets?token=token_here`)
                .send({
                    cost: '',
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('cost_required');
                });
        });

        test('should fail and return "no such advert"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id + 100500}/bets?token=token_here`)
                .send({
                    cost: 100,
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('advert_not_found');
                });
        });

        test('should fail and return "no such advert"', () => {
            return request(server)
                .post(`/api/ads/${inactiveAd.id}/bets?token=token_here`)
                .send({
                    cost: 2000,
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('advert_not_found');
                });
        });

        test('should fail and return "can not bet your own ad"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/bets?token=token_here`)
                .send({
                    cost: 2000,
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('can_not_bet_your_own_ad');
                });
        });

        test('should fail and return "cost is not in category"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/bets?token=token2_here`)
                .send({
                    cost: 100,
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('cost_not_in_category_range');
                });
        });

        test('should add bet to advert', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/bets?token=token2_here`)
                .send({
                    cost: 2000,
                })
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.status).toBe(200);
                });
        });

        test('should fail and return "already have bet"', () => {
            return request(server)
                .post(`/api/ads/${createdAd.id}/bets?token=token2_here`)
                .send({
                    cost: 2000,
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('already_have');
                });
        });

        test('should fail and return "category without bets"', () => {
            return request(server)
                .post(`/api/ads/${createdAd2.id}/bets?token=token2_here`)
                .send({
                    cost: 2000,
                })
                .expect('Content-Type', /application\/json/)
                .expect(400)
                .then((res) => {
                    expect(res.body.status).toBe(400);
                    expect(res.body.desc).toBe('category_without_bets');
                });
        });
    });

    describe('Receiving', () => {
        describe('Own', () => {
            test('should return own bets list', () => {
                return request(server)
                    .get(`/api/bets/own?token=token2_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(200)
                    .then((res) => {
                        expect(res.body.status).toBe(200);
                        expect(res.body.bets).toHaveLength(1);

                        expect(res.body.bets[0]).toMatchObject({
                            cost: 2000,
                            active: true,
                        });
                        expect(res.body.bets[0]).toEqual(expect.objectContaining({
                            id: expect.any(Number),
                            UserId: expect.any(Number),
                            AdvertId: expect.any(Number),
                            User: expect.any(Object),
                            Advert: expect.any(Object),
                        }));

                        assertAdvert(res.body.bets[0].Advert);
                    });
            });
        });

        describe('Own adverts', () => {
            test('should return bets list of one own advert', () => {
                return request(server)
                    .get(`/api/ads/${createdAd.id}/bets?token=token_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(200)
                    .then((res) => {
                        expect(res.body.status).toBe(200);
                        expect(res.body.bets).toHaveLength(1);

                        expect(res.body.bets[0]).toMatchObject({
                            cost: 2000,
                            active: true,
                        });
                        expect(res.body.bets[0]).toEqual(expect.objectContaining({
                            id: expect.any(Number),
                            UserId: expect.any(Number),
                            AdvertId: expect.any(Number),
                            User: expect.any(Object),
                        }));

                        assertUser(res.body.bets[0].User);
                    });
            });

            test('should fail and return "advert not found"', () => {
                return request(server)
                    .get(`/api/ads/${createdAd.id + 100500}/bets?token=token_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(400)
                    .then((res) => {
                        expect(res.body.status).toBe(400);
                        expect(res.body.desc).toBe('advert_not_found');
                    });
            });

            test('should fail and return "advert not found" because of status', () => {
                return request(server)
                    .get(`/api/ads/${inactiveAd.id}/bets?token=token_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(400)
                    .then((res) => {
                        expect(res.body.status).toBe(400);
                        expect(res.body.desc).toBe('advert_not_found');
                    });
            });

            test('should fail and return "not your advert"', () => {
                return request(server)
                    .get(`/api/ads/${createdAd.id}/bets?token=token2_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(400)
                    .then((res) => {
                        expect(res.body.status).toBe(400);
                        expect(res.body.desc).toBe('not_your_ad');
                    });
            });

            test('should return bets list of all own adverts', () => {
                return request(server)
                    .get(`/api/bets/adverts?token=token_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(200)
                    .then((res) => {
                        expect(res.body.status).toBe(200);
                        expect(res.body.bets).toHaveLength(1);

                        expect(res.body.bets[0]).toMatchObject({
                            cost: 2000,
                            active: true,
                        });
                        expect(res.body.bets[0]).toEqual(expect.objectContaining({
                            id: expect.any(Number),
                            UserId: expect.any(Number),
                            AdvertId: expect.any(Number),
                        }));

                        assertAdvert(res.body.bets[0].Advert);
                    });
            });

            test('should fail and return "no adverts found"', () => {
                return request(server)
                    .get(`/api/bets/adverts?token=token2_here`)
                    .expect('Content-Type', /application\/json/)
                    .expect(400)
                    .then((res) => {
                        expect(res.body.status).toBe(400);
                        expect(res.body.desc).toBe('no_adverts_found');
                    });
            });
        });
    });
});
