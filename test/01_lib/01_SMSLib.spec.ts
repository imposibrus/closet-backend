import * as smsLib from '../../src/lib/SMSLib';

describe('SMSLib', () => {
    test('should generate sms-code', () => {
        let code = smsLib.generate();

        expect(code).toBeGreaterThanOrEqual(1000);
        expect(code).toBeLessThanOrEqual(9999);
    });
});
