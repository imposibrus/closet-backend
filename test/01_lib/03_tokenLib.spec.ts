import {generate} from '../../src/lib/tokenLib';

describe('tokenLib', () => {
    test('should generate token with given length', () => {
        expect(generate(10)).toHaveLength(10);
    });
});
