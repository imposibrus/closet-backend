import {ValidationError} from 'sequelize';
import * as models from '../../src/models';

describe('Category model', () => {
    let categoryInstance: models.Category;

    beforeEach(() => {
        categoryInstance = new models.Category({
            title: '1',
            costFrom: 100,
            costUntil: 200,
        });
    });

    test('should `betsActive` be true by default', () => {
        expect(categoryInstance.betsActive).toBe(true);
    });

    describe('validation', () => {
        let categoryInstance: models.Category;

        beforeEach(() => {
            categoryInstance = new models.Category({
                title: '1',
                costFrom: 200,
                costUntil: 100,
            });
        });

        test('should check cost from and cost until', () => {
            return categoryInstance.validate().catch((err: ValidationError) => {
                expect(err).toHaveProperty('errors');
                expect(err.errors).toHaveLength(1);
                expect(err.errors[0]).toHaveProperty('path', 'costRange');
            });
        });
    });
});
