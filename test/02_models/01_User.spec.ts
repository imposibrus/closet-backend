import {ValidationError} from 'sequelize';
import * as models from '../../src/models';

describe('User model', () => {
    let userInstance: models.User;

    beforeEach(() => {
        userInstance = new models.User({
            firstName: 'Vadim',
            lastName: 'Petrov',
            phone: '1234567890',
            city: 'Kazan',
            password: '123'
        });
    });

    test(
        'should getter method `fullName` return sum of `firstName` and `lastName`',
        () => {
            expect(userInstance.fullName).toBe('Vadim Petrov');
        }
    );

    test('should setter method `fullName` set `firstName` and `lastName`', () => {
        userInstance.fullName = 'Yuri Nikolaev';
        expect(userInstance.firstName).toBe('Yuri');
        expect(userInstance.lastName).toBe('Nikolaev');
        expect(userInstance.fullName).toBe('Yuri Nikolaev');
    });

    test('should `activated` prop set to false by default', () => {
        expect(userInstance.activated).toBe(false);
    });

    describe('required fields', () => {
        let userInstanceWithoutPassword: models.User, userInstanceWithoutPhone: models.User;

        beforeAll(() => {
            userInstanceWithoutPassword = new models.User({
                firstName: 'Vadim',
                lastName: 'Petrov',
                city: 'Kazan',
                phone: '1234567890'
            });

            userInstanceWithoutPhone = new models.User({
                firstName: 'Vadim',
                lastName: 'Petrov',
                city: 'Kazan',
                password: '123'
            });
        });

        test('should phone be required', () => {
            return userInstanceWithoutPhone.validate().catch((err: ValidationError) => {
                expect(err).toHaveProperty('errors');
                expect(err.errors).toHaveLength(1);
                expect(err.errors[0]).toHaveProperty('path', 'phone');
            });
        });

        test('should password be required', () => {
            return userInstanceWithoutPassword.validate().catch((err: ValidationError) => {
                expect(err).toHaveProperty('errors');
                expect(err.errors).toHaveLength(1);
                expect(err.errors[0]).toHaveProperty('path', 'password');
            });
        });
    });
});
