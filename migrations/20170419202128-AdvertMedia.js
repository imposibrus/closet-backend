'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        const sequelize = queryInterface.sequelize;

        return sequelize.transaction((t) => {
            return queryInterface.addColumn('Adverts', 'mediaIds', Sequelize.ARRAY(Sequelize.INTEGER), {transaction: t}).then(() => {
                return queryInterface.removeColumn('Adverts', 'photoId', {transaction: t});
            });
        });
    },

    down: function (queryInterface, Sequelize) {
        const sequelize = queryInterface.sequelize;

        return sequelize.transaction((t) => {
            return queryInterface.removeColumn('Adverts', 'mediaIds', {transaction: t}).then(() => {
                return queryInterface.addColumn('Adverts', 'photoId', Sequelize.INTEGER, {transaction: t});
            });
        });
    }
};
