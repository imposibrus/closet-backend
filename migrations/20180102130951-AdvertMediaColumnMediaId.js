'use strict';

module.exports = {
    up: function (queryInterface) {
        return queryInterface.renameColumn('AdvertMedia', 'MediumId', 'MediaId');
    },

    down: function (queryInterface) {
        return queryInterface.renameColumn('AdvertMedia', 'MediaId', 'MediumId');
    }
};
