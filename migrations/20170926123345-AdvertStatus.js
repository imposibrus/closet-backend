'use strict';

const config = require('../dst/src/lib/config').default,
    advertStatusesKeys = Object.keys(config.get('advertStatuses'));

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.addColumn('Adverts', 'status', {
            type: Sequelize.ENUM(...advertStatusesKeys),
            defaultValue: advertStatusesKeys[0],
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.removeColumn('Adverts', 'status', {transaction: t}),
                queryInterface.sequelize.query('DROP TYPE IF EXISTS "enum_Adverts_status";', {transaction: t}),
            ]);
        });
    }
};
