'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('Media', 'meta', {type: Sequelize.JSONB});
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.removeColumn('Media', 'meta');
  }
};
