'use strict';

const Promise = require('bluebird'),
    models = require('../dst/src/models');

module.exports = {
    up: function (queryInterface, Sequelize) {
        const sequelize = queryInterface.sequelize;

        return sequelize.transaction((t) => {
            return models.Advert.findAll({
                attributes: ['id', 'mediaIds'],
                where: {
                    mediaIds: {
                        $ne: null,
                    },
                },
                transaction: t,
            }).then((foundAdverts) => {
                return Promise.resolve(foundAdverts).mapSeries((foundAdvert) => {
                    return foundAdvert.setMedia(foundAdvert.mediaIds, {transaction: t});
                });
            });
        });
    },

    down: function (queryInterface, Sequelize) {
        const sequelize = queryInterface.sequelize;

        return sequelize.transaction((t) => {
            return models.Advert.findAll({
                attributes: ['id', 'mediaIds'],
                transaction: t,
            }).then((foundAdverts) => {
                return Promise.resolve(foundAdverts).mapSeries((foundAdvert) => {
                    return foundAdvert.setMedia([], {transaction: t});
                });
            });
        });
    }
};
