
import * as express from 'express';
import * as controllers from '../../controllers';
import * as tokenLib from '../../lib/tokenLib';
import _logger from '../../lib/logger';
import RequestError from '../../lib/RequestError';

const router: express.Router = express.Router(),
    logger = _logger.getLogger('routes/api');

router.post('/registration', controllers.authRegistrationController);
router.post('/resend_code', controllers.authReSendController);
router.post('/check_code', controllers.authCheckCodeController);
router.post('/login', controllers.authLoginController);

router.post('/ads/:advertId/complaint', tokenLib.check(), controllers.complaintCreateController);

router.post('/ads', tokenLib.check(), controllers.advertCreateController);
router.get('/ads', tokenLib.check(false), controllers.customizableAdsListController({}, true));
router.get('/ads/my', tokenLib.check(), (req, res, next) => {
    controllers.customizableAdsListController({UserId: req.user.id}, false)(req, res, next);
});
router.get('/ads/:advertId', tokenLib.check(false), controllers.advertGetOneController);
router.put('/ads/:advertId', tokenLib.check(), controllers.advertUpdateController);

router.post('/ads/:advertId/bets', tokenLib.check(), controllers.betCreateController);
router.get('/ads/:advertId/bets', tokenLib.check(), controllers.betListOfOneMyAdvertController);
router.get('/bets/own', tokenLib.check(), controllers.betOwnListController);
router.get('/bets/adverts', tokenLib.check(), controllers.betForOwnAdvertsListController);

router.get('/categories', tokenLib.check(false), controllers.categoryListController);
router.get('/categories/:categoryId/ads', tokenLib.check(false), (req, res, next) => {
    controllers.customizableAdsListController({CategoryId: req.params.categoryId}, false)(req, res, next);
});

router.post('/ads/:advertId/favorite', tokenLib.check(), controllers.favoriteCreateController);
router.get('/favorites', tokenLib.check(), controllers.favoriteListController);
router.delete('/favorites/:advertId', tokenLib.check(), controllers.favoriteDeleteController);

router.post('/ads/:advertId/questions', tokenLib.check(), controllers.questionCreateController);
router.get('/ads/:advertId/questions', tokenLib.check(false), controllers.questionListController);
router.post('/questions/:questionId/answer', tokenLib.check(), controllers.questionCreateAnswerController);

router.post('/upload', tokenLib.check(), controllers.uploadController);

router.get('/search', controllers.searchController);

router.use((req, res, next) => {
    const err = new RequestError('Not Found');

    err.status = 404;
    next(err);
});

router.use((err: RequestError, req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.critical(err);
    res.status(err.status ?? 500);
    res.send({status: err.status ?? 500, message: err.message, error: err});
});

export default router;
