/* istanbul ignore file */

import {Dictionary} from 'lodash';
import {Transaction} from 'sequelize';
import * as stripTags from 'striptags';
import * as models from './models';
import config from './lib/config';
import User from './models/User';
import Advert from './models/Advert';
import Category from './models/Category';
import Question from './models/Question';
import Complaint from './models/Complaint';

const complaintTypes = config.get('complaintTypes'),
    advertStatuses = config.get('advertStatuses'),
    dictionaryToOptions = (dictionary: Dictionary<string>) => Object.keys(dictionary).map((key) => ({val: key, title: dictionary[key]}));

export interface SelectOption {
    title: string;
    val: string;
}

export default {
    fieldActions: [
        {
            type: 'link',
            text: 'Редактировать',
            classes: 'btn btn-default',
            //target: '_blank',
            href(this: any, collectionsName: string) {
                return '/admin/edit/'+ collectionsName +'/' + this.id;
            }
        },
        {
            html(this: any, collectionsName: string) {
                return '<a href="/admin/delete/'+ collectionsName +'/'+ this.id +'" class="delete">Удалить</a>';
            }
        }
    ],
    listActions: [
        {
            html(modelConfig: any) {
                let res = `<a href="/admin/edit/${modelConfig.name}/new" class="btn btn-success">Добавить</a>`;

                if (modelConfig.hasSorting) {
                    res += ` <a href="/admin/sort/${modelConfig.name}" class="btn btn-default">Сортировать</a>`;
                }

                return res;
            }
        }
    ],
    collections: [
        {
            name: 'users',
            model: 'User',
            label: 'Пользователи',
            // populate: ['Photo'],
            // hasSorting: true,
            fields: {
                firstName: {
                    type: 'text',
                    label: 'Имя'
                },
                lastName: {
                    type: 'text',
                    label: 'Фамилия'
                },
                phone: {
                    type: 'text',
                    label: 'Телефон'
                },
                city: {
                    type: 'text',
                    label: 'Город'
                },
                password: {
                    type: 'text',
                    label: 'Пароль',
                    setHandler: (body: any, item: User) => {
                        if (!item.previous('password')) {
                            item.password = body.password;
                        }
                    }
                },
                activated: {
                    type: 'checkbox',
                    label: 'Активирован?',
                    default: '1',
                    setHandler: (body: any, item: User) => {
                        item.set('activated', body.activated === '1');
                    }
                }
            },
            list: {
                fields: [
                    {
                        label: 'Имя',
                        field: 'firstName'
                    },
                    {
                        label: 'Фамилия',
                        field: 'lastName'
                    },
                    {
                        label: 'Телефон',
                        field: 'phone'
                    },
                    {
                        label: 'Город',
                        field: 'city'
                    },
                    {
                        label: 'Активирован?',
                        value: (item: User) => {
                            if (item.activated) {
                                return 'Да';
                            }

                            return 'Нет';
                        },
                    }
                ]
            }
        },
        {
            name: 'adverts',
            model: 'Advert',
            label: 'Объявления',
            populate: ['User', 'Category'],
            fields: {
                title: {
                    type: 'text',
                    label: 'Заголовок'
                },
                status: {
                    type: 'select',
                    label: 'Статус',
                    options: dictionaryToOptions(advertStatuses),
                },
                text: {
                    type: 'textarea',
                    label: 'Текст'
                },
                coordinates: {
                    type: 'coordinates',
                    label: 'Координаты',
                    setHandler: (body: any, item: Advert) => {
                        item.coordinates = {
                            type: 'Point',
                            coordinates: body.coordinates
                        };
                    }
                },
                // wouldDeletedAt: {
                //     type: 'date',
                //     label: 'Будет удален в'
                // },
                UserId: {
                    type: 'select',
                    label: 'Пользователь',
                    options: (cb: (err?: Error|null, results?: SelectOption[]) => void) => {
                        models.User.findAll().then((foundUsers: models.User[]) => {
                            cb(null, foundUsers.map((foundUser: models.User) => {
                                return {title: foundUser.fullName, val: foundUser.id};
                            }));
                        }).catch(cb);
                    },
                },
                CategoryId: {
                    type: 'select',
                    label: 'Категория',
                    options: (cb: (err?: Error|null, results?: SelectOption[]) => void) => {
                        models.Category.findAll().then((foundCategories: models.Category[]) => {
                            cb(null, foundCategories.map((foundCategory: models.Category) => {
                                return {title: foundCategory.title, val: foundCategory.id};
                            }));
                        }).catch(cb);
                    },
                },
                mediaIds: {
                    type: 'image',
                    label: 'Фото',
                    previews: {
                        '150x150': {
                            width: 150,
                            height: 150,
                        },
                    },
                    array: true,
                    setHandler: async (body: any, item: Advert) => {
                        await models.sequelize.transaction(async (t: Transaction) => {
                            if (!body.mediaIds) {
                                await item.$set('Media', [], {transaction: t});
                            } else {
                                await item.$set('Media', body.mediaIds.split(','), {transaction: t});
                            }
                        });
                    },
                    getHandler: (item: Advert): Promise<models.Media | models.Media[]> => {
                        return item.$get('Media');
                    },
                },
            },
            list: {
                fields: [
                    {
                        label: 'Заголовок',
                        field: 'title',
                    },
                    {
                        label: 'Статус',
                        value: (item: Advert) => {
                            return advertStatuses[item.status];
                        },
                    },
                    {
                        label: 'Текст',
                        field: 'text',
                    },
                    {
                        label: 'Координаты',
                        value: (item: Advert) => {
                            return item.coordinates?.coordinates?.join(', ');
                        },
                    },
                    {
                        label: 'Будет удален в (не работает)',
                        field: 'wouldDeletedAt',
                    },
                    {
                        label: 'Пользователь',
                        value: (item: Advert) => {
                            const user = item.User;

                            if (user) {
                                return `<a href="/admin/edit/users/${user.id}" target="_blank">${user.fullName}</a>`;
                            }

                            return '---';
                        },
                    },
                    {
                        label: 'Категория',
                        value: (item: Advert) => {
                            const category = item.Category;

                            if (category) {
                                return `<a href="/admin/edit/categories/${category.id}" target="_blank">${category.title}</a>`;
                            }

                            return '---';
                        },
                    },
                ],
            },
        },
        {
            name: 'categories',
            model: 'Category',
            label: 'Категории',
            // populate: ['User'],
            hasSorting: true,
            fields: {
                title: {
                    type: 'text',
                    label: 'Заголовок',
                },
                costFrom: {
                    type: 'number',
                    label: 'Цена от',
                },
                costUntil: {
                    type: 'number',
                    label: 'Цена до',
                },
                betsActive: {
                    type: 'checkbox',
                    label: 'Ставки активны',
                    default: '1',
                    setHandler: (body: any, item: Category) => {
                        item.set('betsActive', body.betsActive === '1');
                    }
                },
            },
            list: {
                fields: [
                    {
                        label: 'Заголовок',
                        field: 'title',
                    },
                    {
                        label: 'Цена от',
                        field: 'costFrom',
                    },
                    {
                        label: 'Цена до',
                        field: 'costUntil',
                    },
                    {
                        label: 'Ставки активны',
                        value: (item: Category) => {
                            if (item.betsActive) {
                                return 'Да';
                            }

                            return 'Нет';
                        },
                    },
                ],
            },
        },
        {
            name: 'question',
            model: 'Question',
            label: 'Вопросы',
            populate: ['User', 'Advert'],
            // hasSorting: true,
            fields: {
                questionText: {
                    type: 'textarea',
                    label: 'Вопрос',
                },
                answerText: {
                    type: 'textarea',
                    label: 'Ответ',
                },
                date: {
                    type: 'date',
                    label: 'Дата',
                    default: () => {
                        return Math.floor(Date.now() / 1000);
                    },
                },
                UserId: {
                    type: 'select',
                    label: 'Пользователь',
                    options: (cb: (err?: Error|null, results?: SelectOption[]) => void) => {
                        models.User.findAll({attributes: ['id', 'firstName', 'lastName']}).then((foundUsers: models.User[]) => {
                            cb(null, foundUsers.map((foundUser: models.User) => {
                                return {title: foundUser.fullName, val: foundUser.id};
                            }));
                        }).catch(cb);
                    },
                },
                AdvertId: {
                    type: 'select',
                    label: 'Объявление',
                    options: (cb: (err?: Error|null, results?: SelectOption[]) => void) => {
                        models.Advert.findAll({attributes: ['id', 'title']}).then((foundAdverts: models.Advert[]) => {
                            cb(null, foundAdverts.map((foundAdvert: models.Advert) => {
                                return {title: foundAdvert.title, val: foundAdvert.id};
                            }));
                        }).catch(cb);
                    },
                },
            },
            list: {
                fields: [
                    {
                        label: 'Вопрос',
                        value: (item: Question) => {
                            let text = item.questionText ?? '---';

                            if (text.length > 300) {
                                text = text.substr(0, 300) + '...';
                            }

                            return text;
                        },
                    },
                    {
                        label: 'Ответ',
                        value: (item: Question) => {
                            let text = item.answerText ?? '---';

                            if (text.length > 300) {
                                text = text.substr(0, 300) + '...';
                            }

                            return text;
                        },
                    },
                    {
                        label: 'Пользователь',
                        value: (item: Question) => {
                            const user = item.User;

                            if (user) {
                                return `<a href="/admin/edit/users/${user.id}" target="_blank">${user.fullName}</a>`;
                            }

                            return '---';
                        },
                    },
                    {
                        label: 'Объявление',
                        value: (item: Question) => {
                            const advert = item.Advert;

                            if (advert) {
                                return `<a href="/admin/edit/adverts/${advert.id}" target="_blank">${advert.title}</a>`;
                            }

                            return '---';
                        },
                    },
                ],
            },
        },
        {
            name: 'complaints',
            model: 'Complaint',
            label: 'Жалобы',
            populate: ['User', 'Advert'],
            fields: {
                type: {
                    type: 'select',
                    label: 'Тип',
                    options: dictionaryToOptions(complaintTypes),
                },
                text: {
                    type: 'textarea',
                    label: 'Текст',
                },
                UserId: {
                    type: 'select',
                    label: 'Пользователь',
                    options: (cb: (err?: Error|null, results?: SelectOption[]) => void) => {
                        models.User.findAll({attributes: ['id', 'firstName', 'lastName']}).then((foundUsers: models.User[]) => {
                            cb(null, foundUsers.map((foundUser: models.User) => {
                                return {title: foundUser.fullName, val: foundUser.id};
                            }));
                        }).catch(cb);
                    },
                },
                AdvertId: {
                    type: 'select',
                    label: 'Объявление',
                    options: (cb: (err?: Error|null, results?: SelectOption[]) => void) => {
                        models.Advert.findAll({attributes: ['id', 'title']}).then((foundAdverts: models.Advert[]) => {
                            cb(null, foundAdverts.map((foundAdvert: models.Advert) => {
                                return {title: foundAdvert.title, val: foundAdvert.id};
                            }));
                        }).catch(cb);
                    },
                },
            },
            list: {
                fields: [
                    {
                        label: 'Тип',
                        value: (item: Complaint) => {
                            return complaintTypes[item.type];
                        },
                    },
                    {
                        label: 'Текст',
                        value: (item: Complaint) => {
                            return stripTags(item.text);
                        },
                    },
                    {
                        label: 'Пользователь',
                        value: (item: Complaint) => {
                            const user = item.User;

                            if (user) {
                                return `<a href="/admin/edit/users/${user.id}" target="_blank">${user.fullName}</a>`;
                            }

                            return '---';
                        },
                    },
                    {
                        label: 'Объявление',
                        value: (item: Complaint) => {
                            const advert = item.Advert;

                            if (advert) {
                                return `<a href="/admin/edit/adverts/${advert.id}" target="_blank">${advert.title}</a>`;
                            }

                            return '---';
                        },
                    },
                ],
            },
        },
    ],
};
