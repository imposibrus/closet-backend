
import {
    Table, Column, Model, DataType, BelongsToMany
} from 'sequelize-typescript';
import {cloneDeep, pick} from 'lodash';
import config from '../lib/config';
import Advert from './Advert';
import AdvertMedia from './AdvertMedia';
import pathsHelper from '../pathsHelper';

const toAbsoluteUrl = (path: string) => {
    return `http://${config.get('domain')}${path.replace(pathsHelper.documentRoot, '')}`;
};

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Media',
})
export default class Media extends Model<Media> {
    @Column
    public path!: string;

    @Column
    public name!: string;

    @Column(DataType.ENUM('image', 'file'))
    public mediaType!: string;

    @Column(DataType.JSONB)
    public meta: any;

    get url() {
        return toAbsoluteUrl(this.path);
    }

    @BelongsToMany(() => Advert, () => AdvertMedia)
    public Adverts?: Advert[];

    public toJSON() {
        const plainObj = cloneDeep(this.get() as Omit<this, 'url'> & {url: string, previews: object}),
            allowedAttributes = ['id', 'url', 'previews'];

        plainObj.url = this.url;
        plainObj.previews = plainObj.meta.previews;

        if (plainObj.previews) {
            for (const preview in plainObj.previews) {
                if (plainObj.previews.hasOwnProperty(preview)) {
                    plainObj.previews[preview] = toAbsoluteUrl(plainObj.previews[preview].path);
                }
            }
        }

        return pick(plainObj, allowedAttributes);
    }
}
