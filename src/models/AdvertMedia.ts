
import {Table, Column, Model, ForeignKey} from 'sequelize-typescript';
import Advert from './Advert';
import Media from './Media';

@Table({
    timestamps: true,
    freezeTableName: true,
    tableName: 'AdvertMedia',
})
export default class AdvertMedia extends Model<AdvertMedia> {
    @ForeignKey(() => Media)
    @Column
    public MediaId!: number;

    @ForeignKey(() => Advert)
    @Column
    public AdvertId!: number;
}
