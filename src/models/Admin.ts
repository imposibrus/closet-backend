/* istanbul ignore file */
import {
    Table, Column, Model, AllowNull, NotEmpty, BeforeCreate, BeforeUpdate, Unique
} from 'sequelize-typescript';
import {cloneDeep, omit} from 'lodash';
import hashPassword from '../lib/hashPassword';

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Admins',
})
export default class Admin extends Model<Admin> {
    @AllowNull(false)
    @NotEmpty
    @Unique
    @Column
    public login!: string;

    @AllowNull(false)
    @NotEmpty
    @Column
    public password!: string;

    public passwordMismatch(pass: string) {
        return this.password !== hashPassword(pass);
    }

    @BeforeCreate
    @BeforeUpdate
    public static userPasswordChange(admin: Admin) {
        if (admin.changed('password')) {
            admin.password = hashPassword(admin.password);
        }
    }

    public static hashPassword(plainPassword: string) {
        return hashPassword(plainPassword);
    }

    public toJSON() {
        const plainObj = cloneDeep(this.get()),
            privateAttributes: string[] = ['createdAt', 'updatedAt', 'deletedAt', 'password'];

        return omit(plainObj, privateAttributes);
    }
}
