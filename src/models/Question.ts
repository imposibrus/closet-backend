
import {
    Table, Column, Model, AllowNull, NotEmpty, Default, DataType, BelongsTo, ForeignKey
} from 'sequelize-typescript';
import {cloneDeep, omit} from 'lodash';
import User from './User';
import Advert from './Advert';

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Questions',
})
export default class Question extends Model<Question> {
    @AllowNull(false)
    @NotEmpty
    @Column(DataType.TEXT)
    public questionText!: string;

    @Column(DataType.TEXT)
    public answerText!: string;

    @Default(Date.now)
    @Column(DataType.DATE)
    public date!: Date;

    @ForeignKey(() => User)
    @Column
    public UserId!: number;

    @ForeignKey(() => Advert)
    @Column
    public AdvertId!: number;

    @BelongsTo(() => User)
    public User?: User;

    @BelongsTo(() => Advert)
    public Advert?: Advert;

    public toJSON() {
        const plainObj = cloneDeep(this.get() as Omit<this, 'date'> & {date: number}),
            privateAttributes = ['AdvertId', 'UserId', 'createdAt', 'updatedAt', 'deletedAt'];

        plainObj.User = this.User;
        plainObj.date = Math.floor(this.date.getTime() / 1000);

        return omit(plainObj, privateAttributes);
    }
}
