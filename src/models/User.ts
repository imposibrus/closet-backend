
import {
    Table, Column, Model, AllowNull, NotEmpty, Default, BeforeCreate, BeforeUpdate, Unique, HasMany, BelongsToMany
} from 'sequelize-typescript';
import {cloneDeep, omit} from 'lodash';
import hashPassword from '../lib/hashPassword';
import Advert from './Advert';
import Favorite from './Favorite';
import Question from './Question';
import Complaint from './Complaint';
import Bet from './Bet';

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Users',
})
export default class User extends Model<User> {
    @Column
    public firstName!: string;

    @Column
    public lastName!: string;

    @AllowNull(false)
    @NotEmpty
    @Unique
    @Column
    public phone!: string;

    @Column
    public code!: number;

    @Column
    public city!: string; //fixme: make separate table for cities and coordinates

    @AllowNull(false)
    @NotEmpty
    @Column
    public password!: string;

    @Default(false)
    @Column
    public activated!: boolean;

    @HasMany(() => Advert)
    public Adverts?: Advert[];

    @BelongsToMany(() => Advert, () => Favorite)
    public favorites?: Advert[];

    @HasMany(() => Question)
    public Questions?: Question[];

    @HasMany(() => Complaint)
    public Complaints?: Complaint[];

    @HasMany(() => Bet)
    public Bets?: Bet[];

    get fullName() {
        return this.firstName + ' ' + this.lastName
    }
    set fullName(value: string) {
        const names = value.split(' ');

        this.setDataValue('firstName', names.slice(0, -1).join(' '));
        this.setDataValue('lastName', names.slice(-1).join(' '));
    }

    public toJSON() {
        const plainObj = cloneDeep(this.get()),
            privateAttributes: string[] = ['password', 'code', 'activated', 'fullName', 'createdAt', 'updatedAt', 'deletedAt'];

        return omit(plainObj, privateAttributes);
    }

    @BeforeCreate
    @BeforeUpdate
    public static userPasswordChange(user: User) {
        if (user.changed('password')) {
            user.password = hashPassword(user.password);
        }
    }
}
