
import {
    Table, Column, Model, AllowNull, NotEmpty, DataType, Default, BelongsTo, ForeignKey, BelongsToMany, HasMany
} from 'sequelize-typescript';
import {cloneDeep, omit, map} from 'lodash';
import {Transaction} from 'sequelize';
import config from '../lib/config';
import User from './User';
import Favorite from './Favorite';
import Question from './Question';
import AdvertMedia from './AdvertMedia';
import Media from './Media';
import Category from './Category';
import Complaint from './Complaint';
import Bet from './Bet';

const advertStatusesKeys = Object.keys(config.get('advertStatuses'));

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Adverts',
})
export default class Advert extends Model<Advert> {
    @AllowNull(false)
    @NotEmpty
    @Column
    public title!: string;

    @Column
    public text!: string;

    @Column(DataType.GEOMETRY('POINT'))
    public coordinates: any;

    @Default(() => {
        // TODO: write cron task for changing status of old ads
        const date = new Date();

        date.setDate(date.getDate() + config.get('advExpireDays'));

        return date;
    })
    @Column(DataType.DATE)
    public wouldDeletedAt!: Date;

    @Column(DataType.ARRAY(DataType.INTEGER))
    public mediaIds!: number[];

    @Default(advertStatusesKeys[0])
    @Column(DataType.ENUM(...advertStatusesKeys))
    public status!: string;

    public isFavorite?: boolean;
    public questionsCount?: number;

    @ForeignKey(() => User)
    @Column
    public UserId!: number;

    @ForeignKey(() => Category)
    @Column
    public CategoryId!: number;

    @BelongsTo(() => User)
    public User?: User;

    @BelongsToMany(() => User, () => Favorite)
    public hasFavorite?: User[];

    @HasMany(() => Question)
    public Questions?: Question[];

    @BelongsToMany(() => Media, () => AdvertMedia)
    public Media?: Media[];

    @BelongsTo(() => Category)
    public Category?: Category;

    @HasMany(() => Complaint)
    public Complaints?: Complaint[];

    @HasMany(() => Bet)
    public Bets?: Bet[];

    public toJSON() {
        const plainObj = cloneDeep(this.get() as this & {images: any}),
            privateAttributes = [
                'wouldDeletedAt', 'mediaIds', 'UserId', 'CategoryId', 'Media', 'Favorites',
                'createdAt', 'updatedAt', 'deletedAt'
            ];

        if (Array.isArray(plainObj.coordinates?.coordinates)) {
            plainObj.coordinates = plainObj.coordinates.coordinates.reverse();
        }

        plainObj.images = this.Media;
        plainObj.isFavorite = this.isFavorite;
        plainObj.questionsCount = this.questionsCount;

        return omit(plainObj, privateAttributes);
    }

    public static async prepareForApi(user: User, adverts: Advert[], transaction?: Transaction) {
        const shouldCommit = !transaction,
            t = transaction ?? await (this.sequelize ?? user?.sequelize).transaction();

        try {
            const userFavorites = new Set(user ? map<any>(await user.$get('favorites', {
                attributes: ['id'],
                transaction: t,
            }), 'id') : []);

            for (const ad of adverts) {
                ad.isFavorite = userFavorites.has(ad.id);
                ad.questionsCount = await ad.$count('Questions', {transaction: t}) as any;
            }

            if (shouldCommit) {
                await t.commit();
            }
        } catch (err) {
            if (shouldCommit) {
                await t.rollback();
            }

            throw err;
        }
    }
};
