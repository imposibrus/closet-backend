
import {
    Table, Column, Model, Default, HasMany
} from 'sequelize-typescript';
import {cloneDeep, omit} from 'lodash';
import Advert from './Advert';

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Categories',
    validate: {
        costRange: function(this: Category) {
            if (~~this.costFrom > ~~this.costUntil) {
                let err = new Error('`costFrom` should be smaller than `costUntil`');

                (err as any).fields = ['costFrom', 'costUntil'];

                throw err;
            }
        },
    },
})
export default class Category extends Model<Category> {
    @Column
    public title!: string;

    @Column
    public costFrom!: number;

    @Column
    public costUntil!: number;

    @Default(true)
    @Column
    public betsActive!: boolean;

    @Default(0)
    @Column
    public order!: number;

    get viewForSort() {
        return this.title;
    }

    @HasMany(() => Advert)
    public Adverts?: Advert[];

    public toJSON() {
        const plainObj = cloneDeep(this.get()),
            privateAttributes: string[] = ['viewForSort', 'order', 'createdAt', 'updatedAt', 'deletedAt'];

        return omit(plainObj, privateAttributes);
    }
}
