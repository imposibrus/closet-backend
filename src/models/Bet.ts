
import {
    Table, Column, Model, AllowNull, NotEmpty, Default, BelongsTo, ForeignKey
} from 'sequelize-typescript';
import {cloneDeep, omit} from 'lodash';
import User from './User';
import Advert from './Advert';

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Bets',
    indexes: [
        {
            unique: true,
            fields: ['UserId', 'AdvertId'],
        },
    ],
})
export default class Bet extends Model<Bet> {
    @AllowNull(false)
    @NotEmpty
    @Column
    public cost!: number;

    @Default(true)
    @Column
    public active!: boolean;

    @ForeignKey(() => User)
    @Column
    public UserId!: number;

    @ForeignKey(() => Advert)
    @Column
    public AdvertId!: number;

    @BelongsTo(() => User)
    public User?: User;

    @BelongsTo(() => Advert)
    public Advert?: Advert;

    public toJSON() {
        const plainObj = cloneDeep(this.get()),
            privateAttributes = ['createdAt', 'updatedAt', 'deletedAt'];

        return omit(plainObj, privateAttributes);
    }
}
