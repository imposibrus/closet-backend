
import {
    Table, Column, Model, DataType, BelongsTo, ForeignKey
} from 'sequelize-typescript';
import {cloneDeep, omit} from 'lodash';
import config from '../lib/config';
import Advert from './Advert';
import User from './User';

const complaintTypes = config.get('complaintTypes');

@Table({
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'Complaints',
})
export default class Complaint extends Model<Complaint> {
    @Column(DataType.ENUM(...Object.keys(complaintTypes)))
    public type!: string;

    @Column(DataType.TEXT)
    public text!: string;

    @ForeignKey(() => Advert)
    @Column
    public AdvertId!: number;

    @ForeignKey(() => User)
    @Column
    public UserId!: number;

    @BelongsTo(() => Advert)
    public Advert?: Advert;

    @BelongsTo(() => User)
    public User?: User;

    public toJSON() {
        const plainObj = cloneDeep(this.get()),
            privateAttributes: string[] = ['createdAt', 'updatedAt', 'deletedAt'];

        return omit(plainObj, privateAttributes);
    }
}
