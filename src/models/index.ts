
import {Sequelize} from 'sequelize-typescript';
import config from '../lib/config';
import User from './User';
import Advert from './Advert';
import Admin from './Admin';
import Category from './Category';
import Media from './Media';
import Question from './Question';
import Complaint from './Complaint';
import Bet from './Bet';
import Favorite from './Favorite';
import AdvertMedia from './AdvertMedia';

/* istanbul ignore next */
const logging = process.env.NODE_ENV === 'development' ? (sql: string/*, sequelize*/) => {
        console.error(sql);
    } : config.get('SEQUELIZE_OPTIONS_LOGGING'),
    sequelize = new Sequelize({
        database: config.get('SEQUELIZE_DATABASE'),
        username: config.get('SEQUELIZE_USERNAME'),
        password: config.get('SEQUELIZE_PASSWORD'),
        host: config.get('SEQUELIZE_OPTIONS_HOST'),
        port: config.get('SEQUELIZE_OPTIONS_PORT'),
        dialect: config.get('SEQUELIZE_OPTIONS_DIALECT'),
        logging,
        operatorsAliases: config.get('SEQUELIZE_OPTIONS_OPERATORSALIASES'),
    });

sequelize.addModels([
    User, Advert, Admin, Category, Media, Question, Complaint, Bet, Favorite, AdvertMedia,
]);

/* istanbul ignore next */
const syncing: Promise<void> = sequelize.authenticate().then(() => {
    if (process.env.NODE_ENV === 'development') {
        console.error('DB authenticated');
    }
}).catch((err: Error) => {
    console.error(err);
    process.exit(-1);
});

export {
    sequelize, syncing, User, Advert, Admin, Category, Media, Question, Complaint, Bet, Favorite, AdvertMedia,
};
