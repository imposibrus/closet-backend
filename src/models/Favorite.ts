
import {Table, Column, Model, ForeignKey} from 'sequelize-typescript';
import User from './User';
import Advert from './Advert';

@Table({
    timestamps: true,
    freezeTableName: true,
    tableName: 'Favorites',
})
export default class Favorite extends Model<Favorite> {
    @ForeignKey(() => User)
    @Column
    public UserId!: number;

    @ForeignKey(() => Advert)
    @Column
    public AdvertId!: number;
}
