
import {compact} from 'lodash';

export const normalizeImagesIds = (imagesIds: string[] | string | void) => {
    if (Array.isArray(imagesIds)) {
        return imagesIds;
    }

    return compact((imagesIds || '').split(','));
};
