
import * as Redis from 'ioredis';

import config from './config';

const redis = new Redis({host: config.get('REDIS_HOST')});
let redisReConnectAttempts = 0;

/* istanbul ignore next */
redis.on('error', (err) => {
    redisReConnectAttempts++;

    if (redisReConnectAttempts > 5) {
        throw err;
    }

    console.error(err);
});

export default redis;
