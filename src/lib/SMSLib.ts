import {random} from 'lodash';

export const generate = () => {
    return random(1000, 9999);
};

export const send = async (code: number, phone: string) => {
    return new Promise(setImmediate);
};
