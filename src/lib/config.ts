import * as nconf from 'nconf';
import * as path from 'path';
import * as packageJson from '../../package.json';
import pathsHelper from '../pathsHelper';

nconf.argv()
    .env()
    .file({ file: path.resolve(pathsHelper.documentRoot, 'config.json') });

nconf.set('version', packageJson.version);
nconf.set('name', packageJson.name);

if (nconf.get('NODE_ENV') === 'testing') {
    nconf.set('sequelize:database', 'closet-backend_test');
}

export default nconf;
