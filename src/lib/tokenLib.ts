
import {randomBytes} from 'crypto';
import {RequestHandler} from 'express';
import redis from './redisSharedConnection';
import config from '../lib/config';
import _logger from '../lib/logger';
import * as models from '../models';

const prefix = config.get('name'),
    tokensExpireTime = config.get('tokensExpireTime'),
    logger = _logger.getLogger('lib/tokenLib');

type CheckController = (requiredAuth?: boolean) => RequestHandler;

export const check: CheckController = (requiredAuth = true): RequestHandler => {
    return async (req, res, next) => {
        const token = req.query.token;

        if (!token) {
            if (requiredAuth) {
                return res.status(401).send({status: 401, message: '`token` query parameter is required'});
            } else {
                req.user = null as any;

                return next();
            }
        }

        try {
            const tokenUserId = await redis.get(`${prefix}:tokens:${token}`);

            if (!tokenUserId) {
                return res.status(401).send({status: 401, code: 101, message: 'token is expired'});
            }

            const foundUser = await models.User.findByPk(tokenUserId);

            if (!foundUser) {
                return res.status(401).send({status: 401, code: 102, message: 'user for this token not found'});
            }

            req.user = foundUser;

            await redis.expire(`${prefix}:tokens:${token}`, config.get('tokensExpireTime'));

            next();
        } catch(err) {
            logger.critical(err);
            next(err);
        }
    };
};

export const generate = (tokenLength = 32) => {
    return randomBytes(tokenLength / 2).toString('hex');
};

export const saveUserId = async (userId: string | number) => {
    const token = generate();

    await redis.set(`${prefix}:tokens:${token}`, userId, ['ex', tokensExpireTime]);

    return token;
};
