
import {promisify} from 'util';
import * as _ from 'lodash';
import * as gm from 'gm';

export default async (options: GenImagePreviewsOptions) => {
    const imageObj = gm(options.newFilePath),
        maxWantedEdge = _.max([options.previewParams.width, options.previewParams.height]) || options.previewParams.width,
        imageSize = promisify<gm.Dimensions>(imageObj.size.bind<gm.State, gm.GetterCallback<gm.Dimensions>, gm.State>(imageObj)),
        imageWrite = promisify(imageObj.write.bind(imageObj));

    const size = await imageSize();

    if (options.previewParams.crop === false || options.previewParams.crop === 'false') {
        // scale only
        const maxImageEdge = _.max([size.width, size.height]) || size.width;

        if(maxImageEdge > maxWantedEdge) {
            imageObj
                .resize(options.previewParams.width, options.previewParams.height + '>' as any)
                .gravity('Center');
        }
    } else {
        // scale and crop
        imageObj
            .resize(maxWantedEdge, maxWantedEdge + '^' as any)
            .gravity('Center')
            .crop(options.previewParams.width, options.previewParams.height);
    }

    imageObj.noProfile();

    await imageWrite(options.previewPath);

    return {
        [options.previewParams.width + 'x' + options.previewParams.height]: {
            url: options.previewUrl,
            path: options.previewPath,
        }
    };
};

export interface GenImagePreviewsOptions {
    newFilePath: string;
    previewPath: string;
    previewUrl: string;
    previewParams: {
        width: number;
        height: number;
        crop: boolean | string;
    };
}
