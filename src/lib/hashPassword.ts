
import {createHash} from 'crypto';

export default (password: string) => {
    return createHash('sha512').update(password).digest('hex');
};
