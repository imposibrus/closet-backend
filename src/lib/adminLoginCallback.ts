/* istanbul ignore file */

import {RequestHandler} from 'express';
import * as models from '../models';
import _logger from '../lib/logger';

const logger = _logger.getLogger('adminLoginCallback');

export const handler: RequestHandler = async (req, res, next) => {
    const login = req.body.login,
        pass = req.body.pass;

    if (!req.session) {
        throw new Error('req.session is empty');
    }

    try {
        const foundAdmin = await models.Admin.findOne({where: {login}});

        if (!foundAdmin) {
            logger.debug('Admin with provided login not found.');
            req.session.messages = req.session.messages || [];
            req.session.messages.push({type: 'danger', message: 'Admin with provided login not found.'});
            return res.redirect('/admin/login');
        }

        if (foundAdmin.passwordMismatch(pass)) {
            logger.debug('Password mismatch.');
            req.session.messages = req.session.messages || [];
            req.session.messages.push({type: 'danger', message: 'Password mismatch.'});
            return res.redirect('/admin/login');
        }

        req.session.user = foundAdmin;
        req.session.auth = true;
        res.redirect('/admin');
    } catch (err) {
        next(err);
    }
};
