import * as express from 'express';
import * as path from 'path';
import * as logger from 'morgan';
import * as session from 'express-session';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as _ from 'lodash';
import * as AdminPanel from 'admin-panel';
import * as postNormalize from 'post-normalize';

import config from './lib/config';
import sessionStore from './lib/sessionStore';
import {handler} from './lib/adminLoginCallback';
import * as models from './models';
import routes from './routes';
import adminConfig from './admin-config';
import RequestError from './lib/RequestError';
import pathsHelper from './pathsHelper';

const adminPanel = AdminPanel({
        express,
        models,
        adminConfig,
        storagePath: pathsHelper.storage,
        customControlsDir: path.join(pathsHelper.documentRoot, 'views/admin/controls'),
        loginCallback: handler,
    }),
    app: express.Express = express();

// view engine setup
app.set('views', path.join(pathsHelper.documentRoot, 'views'));
app.set('view engine', 'jade');

/* istanbul ignore next */
if(app.get('env') === 'development') {
    app.use(logger('dev'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
    secret: config.get('sessionSecret'),
    resave: true,
    saveUninitialized: false,
    store: sessionStore
}));

app.use(postNormalize());

app.use('/public', express.static(path.join(pathsHelper.documentRoot, 'public')));

app.use((req, res, next) => {
    config.set('domain', req.hostname);
    res.locals.session = req.session;
    next();
});

app.locals._ = _;
app.locals.config = config;
app.locals.env = app.get('env');

app.use('/', routes);

app.use('/admin', adminPanel);

// catch 404 and forward to error handler
/* istanbul ignore next */
app.use((req, res, next) => {
    const err = new RequestError('Not Found');

    err.status = 404;
    next(err);
});

// error handlers

/* istanbul ignore next */
app.use((err: RequestError, req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.status(err.status ?? 500);

    const error = app.get('env') === 'development' ? err : {};

    if (req.xhr || req.is('json')) {
        res.send({status: err.status ?? 500, message: err.message, error});
        return;
    }

    res.render('error', {
        message: err.message,
        error,
    });
});

export default app;
