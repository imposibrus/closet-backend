
import {RequestHandler} from 'express';
import * as  models from '../models';
import config from '../lib/config';

const complaintTypes = config.get('complaintTypes');

export const complaintCreateController: RequestHandler = async (req, res, next) => {
    const {text, type} = req.body;

    if (!text) {
        res.status(400).send({status: 400, desc: 'text_required'});
        return;
    }

    if (!type) {
        res.status(400).send({status: 400, desc: 'type_required'});
        return;
    }

    if (!Object.prototype.hasOwnProperty.call(complaintTypes, type)) {
        res.status(400).send({status: 400, desc: 'type_invalid'});
        return;
    }

    const t = await models.sequelize.transaction();

    try {
        const foundAd = await models.Advert.findByPk(req.params.advertId, {
            attributes: ['id'],
            transaction: t,
        });

        if (!foundAd) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'no_such_advert'});
            return;
        }

        await foundAd.$create('Complaint', {
            type,
            text,
            UserId: req.user.id,
        }, {transaction: t});
        await t.commit();

        res.send({status: 200});
    } catch (err) {
        await t.rollback();
        next(err);
    }
};
