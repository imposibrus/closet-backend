
import {RequestHandler} from 'express';
import * as  models from '../models';

export const categoryListController: RequestHandler = async (req, res, next) => {
    try {
        const foundCategories = await models.Category.findAll({where: {}, order: [['order', 'ASC']]});

        res.send({status: 200, categories: foundCategories});
    } catch (err) {
        next(err);
    }
};
