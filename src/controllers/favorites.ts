
import {RequestHandler} from 'express';
import * as  models from '../models';

export const favoriteListController: RequestHandler = async (req, res, next) => {
    try {
        const foundFavorites: models.Advert[] = await req.user.$get('favorites', {include: [{model: models.User, as: 'User'}, models.Category, models.Media]});

        await models.Advert.prepareForApi(req.user, foundFavorites);

        res.send({status: 200, favoriteAds: foundFavorites});
    } catch (err) {
        next(err);
    }
};

export const favoriteCreateController: RequestHandler = async (req, res, next) => {
    // TODO: limit favorites list to ~500 items.
    try {
        await req.user.$add('Favorite', req.params.advertId);

        res.send({status: 200});
    } catch (err) {
        next(err);
    }
};

export const favoriteDeleteController: RequestHandler = async (req, res, next) => {
    try {
        await req.user.$remove('Favorite', req.params.advertId);

        res.send({status: 200});
    } catch (err) {
        next(err);
    }
};
