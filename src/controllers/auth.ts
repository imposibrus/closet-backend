
import {RequestHandler} from 'express';
import * as  models from '../models';
import * as smsLib from '../lib/SMSLib';
import * as _ from 'lodash';
import {normalizePhone} from '@codezavod/phone-normalize';
import _logger from '../lib/logger';
import hashPassword from '../lib/hashPassword';
import * as tokenLib from '../lib/tokenLib';

const logger = _logger.getLogger('controllers/auth');

export const authRegistrationController: RequestHandler = async (req, res, next) => {
    const userObj = _.pick(req.body, ['firstName', 'lastName', 'city', 'password', 'phone', 'code']);

    userObj.phone = normalizePhone(userObj.phone);

    if (!userObj.phone || userObj.phone.length !== 11) {
        res.status(400).send({status: 400, desc: 'phone_invalid'});
        return;
    }

    if (!userObj.password) {
        res.status(400).send({status: 400, desc: 'password_invalid'});
        return;
    }

    userObj.code = smsLib.generate();

    try {
        const foundUser = await models.User.findOne({where: {phone: userObj.phone}});

        if (foundUser) {
            res.status(400).send({status: 400, desc: 'phone_duplicate'});
            return;
        }

        const createdUser = await models.User.create(userObj);

        await smsLib.send(createdUser.code, createdUser.phone);

        res.send({status: 200, desc: 'user_registered_code_sent', code: createdUser.code});
    } catch (err) {
        next(err);
    }
};

export const authReSendController: RequestHandler = async (req, res, next) => {
    const phone = normalizePhone(req.body.phone);

    if (phone.length !== 11) {
        res.status(400).send({status: 400, desc: 'phone_invalid'});
        return;
    }

    //TODO: timeout between sends

    try {
        const foundUser = await models.User.findOne({where: {phone}});

        if (!foundUser) {
            res.status(400).send({status: 400, desc: 'user_not_found'});
            return;
        }

        if (foundUser.activated) {
            res.status(400).send({status: 400, desc: 'user_activated'});
            return;
        }

        foundUser.code = smsLib.generate();

        await foundUser.save();
        await smsLib.send(foundUser.code, foundUser.phone);

        res.send({status: 200, desc: 'user_login_code_sent', code: foundUser.code});
    } catch (err) {
        next(err);
    }
};

export const authCheckCodeController: RequestHandler = async (req, res, next) => {
    const phone = normalizePhone(req.body.phone),
        code = ~~req.body.code;

    if (phone.length !== 11) {
        res.status(400).send({status: 400, desc: 'phone_invalid'});
        return;
    }

    try {
        const foundUser = await models.User.findOne({where: {phone, code}});

        if (!foundUser) {
            logger.debug('check_code - user not found', phone, code);
            res.status(400).send({status: 400, desc: 'invalid_code_or_phone'});
            return;
        }

        if (foundUser.activated) {
            res.status(400).send({status: 400, desc: 'user_activated'});
            return;
        }

        await foundUser.update({ activated: true });

        res.send({status: 200});
    } catch (err) {
        next(err);
    }
};

export const authLoginController: RequestHandler = async (req, res, next) => {
    let phone = normalizePhone(req.body.phone),
        password = req.body.password,
        passwordHash = hashPassword(password);

    if (phone.length !== 11) {
        res.status(400).send({status: 400, desc: 'phone_invalid'});
        return;
    }

    if (!password) {
        res.status(400).send({status: 400, desc: 'password_invalid'});
        return;
    }

    try {
        const foundUser = await models.User.findOne({where: {phone, password: passwordHash}});

        if (!foundUser) {
            logger.debug('login - user not found', phone);
            res.status(400).send({status: 400, desc: 'invalid_password_or_phone'});
            return;
        }

        if (!foundUser.activated) {
            res.status(400).send({status: 400, desc: 'user_deactivated'});
            return;
        }

        const token = await tokenLib.saveUserId(foundUser.id);

        res.send({status: 200, token, user: foundUser});
    } catch (err) {
        next(err);
    }
};
