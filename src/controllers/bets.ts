
import {RequestHandler} from 'express';
import {Op} from 'sequelize';
import {map} from 'lodash';
import * as  models from '../models';

export const betOwnListController: RequestHandler = async (req, res, next) => {
    try {
        const foundBets: models.Bet[] = await req.user.$get('Bets', {
            include: [
                models.User,
                {
                    model: models.Advert,
                    include: [{model: models.User, as: 'User'}, models.Category, models.Media],
                },
            ]
        }) as models.Bet[];

        await models.Advert.prepareForApi(req.user, map<models.Bet>(foundBets, 'Advert') as models.Advert[]);

        res.send({status: 200, bets: foundBets});
    } catch (err) {
        next(err);
    }
};

export const betForOwnAdvertsListController: RequestHandler = async (req, res, next) => {
    const t = await models.sequelize.transaction();

    try {
        const ownAdvertsIds: number[] = map<models.Advert>(await req.user.$get('Adverts', {
            where: {status: 'active'},
            attributes: ['id'],
            limit: 500,
            transaction: t,
        }), 'id');

        if (!ownAdvertsIds.length) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'no_adverts_found', message: 'you haven\'t active ads'});
            return;
        }

        const advertsBets = await models.Bet.findAll({
            where: {AdvertId: {[Op.in]: ownAdvertsIds}},
            include: [
                models.User,
                {
                    model: models.Advert,
                    include: [{model: models.User, as: 'User'}, models.Category, models.Media],
                },
            ],
            transaction: t,
        });

        await models.Advert.prepareForApi(req.user, map<models.Bet>(advertsBets, 'Advert') as models.Advert[], t);

        await t.commit();

        res.send({status: 200, bets: advertsBets});
    } catch (err) {
        await t.rollback();
        next(err);
    }
};

export const betCreateController: RequestHandler = async (req, res, next) => {
    if (!req.body.cost) {
        res.status(400).send({status: 400, desc: 'cost_required', message: '`cost` parameter is required'});
        return;
    }

    const t = await models.sequelize.transaction();

    try {
        const foundAdvert = await models.Advert.findByPk(req.params.advertId, {
                attributes: ['id', 'status'],
                include: [
                    {model: models.Category, attributes: ['costFrom', 'costUntil', 'betsActive']},
                    {model: models.User, as: 'User', attributes: ['id']},
                ],
                transaction: t,
            }),
            previousBets = await models.Bet.findOne({
                where: {AdvertId: req.params.advertId},
                attributes: ['id'],
                transaction: t,
            });

        if (!foundAdvert || foundAdvert.status !== 'active' || !foundAdvert.User || !foundAdvert.Category) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'advert_not_found', message: 'advert not found'});
            return;
        }
        if (!foundAdvert.Category.betsActive) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'category_without_bets', message: 'bets are disabled in this category'});
            return;
        }
        if (previousBets) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'already_have', message: 'you already have the bet'});
            return;
        }
        if (foundAdvert.User.id === req.user.id) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'can_not_bet_your_own_ad', message: 'you can not bet you own ad'});
            return;
        }

        const {costFrom, costUntil} = foundAdvert.Category;

        if (req.body.cost < costFrom || req.body.cost > costUntil) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'cost_not_in_category_range', message: '`cost` should be in category cost range'});
            return;
        }

        await req.user.$create('Bet', {AdvertId: req.params.advertId, cost: req.body.cost}, {transaction: t});

        await t.commit();

        res.send({status: 200});
    } catch (err) {
        await t.rollback();
        next(err);
    }
};

export const betListOfOneMyAdvertController: RequestHandler = async (req, res, next) => {
    try {
        const foundAdvert = await models.Advert.findByPk(req.params.advertId, {
            attributes: ['id', 'status'],
            include: [
                {model: models.Bet, include: [models.User]},
                {model: models.User, as: 'User', attributes: ['id']},
            ],
        });

        if (!foundAdvert || !foundAdvert.User || foundAdvert.status !== 'active') {
            res.status(400).send({status: 400, desc: 'advert_not_found', message: 'advert not found'});
            return;
        }

        if (foundAdvert.User.id !== req.user.id) {
            res.status(400).send({status: 400, desc: 'not_your_ad', message: 'this is not your advert'});
            return;
        }

        res.send({status: 200, bets: foundAdvert.Bets});
    } catch (err) {
        next(err);
    }
};
