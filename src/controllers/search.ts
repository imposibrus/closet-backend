
import {RequestHandler} from 'express';
import {Op} from 'sequelize';
import {trim, flatten} from 'lodash';
import {customizableAdsListController} from './ads';

export const searchController: RequestHandler = async (req, res, next) => {
    const search: string = req.query.s;

    if (!search) {
        res.status(400).send({status: 400, desc: 'search_string_required'});
        return;
    }

    customizableAdsListController({
        [Op.and]: [
            {
                [Op.or]: flatten(search.split(' ').map(trim).map(s => [
                    {title: {[Op.iLike]: `%${s}%`}},
                    {text: {[Op.iLike]: `%${s}%`}},
                ])),
            },
        ],
    }, true)(req, res, next);
};
