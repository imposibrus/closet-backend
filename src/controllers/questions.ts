
import {RequestHandler} from 'express';
import * as  models from '../models';

export const questionListController: RequestHandler = async (req, res, next) => {
    try {
        const foundAdvert = await models.Advert.findByPk(req.params.advertId, {attributes: ['id', 'status']});

        if (!foundAdvert || foundAdvert.status !== 'active') {
            res.status(400).send({status: 400, desc: 'no_such_advert'});
            return;
        }

        const advertQuestions = await foundAdvert.$get('Questions', {include: [models.User], order: [['createdAt', 'DESC']]});

        res.send({status: 200, questions: advertQuestions});
    } catch (err) {
        next(err);
    }
};

export const questionCreateController: RequestHandler = async (req, res, next) => {
    const questionText = req.body.questionText && String(req.body.questionText).trim();

    if (!questionText) {
        res.status(400).send({status: 400, desc: 'questionText_required'});
        return;
    }

    const t = await models.sequelize.transaction();

    try {
        const foundQuestion = await models.Question.findOne({where: {questionText}, transaction: t});

        if (foundQuestion) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'question_already_asked'});
            return;
        }

        const foundAdvert = await models.Advert.findByPk(req.params.advertId, {attributes: ['id', 'status'], transaction: t});

        if (!foundAdvert || foundAdvert.status !== 'active') {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'no_such_advert'});
            return;
        }

        const createdQuestion: models.Question = await foundAdvert.$create<models.Question>('Question', {questionText}, {transaction: t}) as models.Question;

        await createdQuestion.$set('User', req.user.id, {transaction: t});

        await t.commit();
        res.send({status: 200});
    } catch (err) {
        await t.rollback();
        next(err);
    }
};

export const questionCreateAnswerController: RequestHandler = async (req, res, next) => {
    const {answerText} = req.body;

    if (!answerText) {
        res.status(400).send({status: 400, desc: 'answerText_is_required'});
        return;
    }

    const t = await models.sequelize.transaction();

    try {
        const foundQuestion = await models.Question.findByPk(req.params.questionId, {
            include: [
                {model: models.Advert, attributes: ['id', 'UserId']}
            ],
            transaction: t,
        });

        if (!foundQuestion || !foundQuestion.Advert) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'question_not_found'});
            return;
        }

        if (foundQuestion.Advert.UserId !== req.user.id) {
            await t.rollback();
            res.status(400).send({status: 400, desc: 'not_your_advert'});
            return;
        }

        await foundQuestion.update({answerText}, {transaction: t});
        await foundQuestion.reload({include: [models.User], transaction: t});

        await t.commit();
        res.send({status: 200, question: foundQuestion});
    } catch (err) {
        await t.rollback();
        next(err);
    }
};
