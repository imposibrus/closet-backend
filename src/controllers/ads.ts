
import {RequestHandler} from 'express';
import {Op} from 'sequelize';
import {toFinite} from 'lodash';
import * as  models from '../models';
import _logger from '../lib/logger';
import {normalizeImagesIds} from '../lib/utils';

const logger = _logger.getLogger('controllers/ads'),
    Sequelize = models.sequelize.Sequelize,
    MAX_RADIUS = 1000000; // 1000km

const checkLat = (lat: number) => {
    return lat >= -90 && lat <= 90;
};
const checkLon = (lon: number) => {
    return lon >= -180 && lon <= 180;
};
const checkRadius = (radius: number) => {
    return radius > 0 && radius <= MAX_RADIUS;
};
const buildWhere = (customWhere: object, geo?: BuildWhereGeo|false) => {
    if (!geo) {
        return Object.assign({}, {status: 'active'}, customWhere);
    } else {
        return Sequelize.and(
            Object.assign({}, {status: 'active'}, customWhere),
            Sequelize.where(
                Sequelize.fn('ST_DWITHIN',
                    Sequelize.cast(Sequelize.col('coordinates'), 'geography'),
                    Sequelize.fn('ST_Buffer',
                        Sequelize.cast(`POINT(${geo.lon} ${geo.lat})`, 'geography'),
                        geo.radius
                    ),
                    0
                ),
                // FIXME: after sequelize update typings goes crazy here
                true as any,
            ),
        );
    }
};

interface BuildWhereGeo {
    lat: number;
    lon: number;
    radius: number;
}

type CustomizableAdsListController = (customWhere?: object, useGeo?: boolean) => RequestHandler

export const customizableAdsListController: CustomizableAdsListController = (customWhere = {}, useGeo = false): RequestHandler => {
    return async (req, res, next) => {
        let limit = ~~req.query.limit || 10;
        const offset = ~~req.query.offset || 0,
            lat = toFinite(req.query.lat),
            lon = toFinite(req.query.lon),
            radius = toFinite(req.query.radius);

        if (useGeo && (!checkLat(lat) || !checkLon(lon) || !checkRadius(radius))) {
            res.status(400).send({
                status: 400,
                desc: 'one_of_lat|lon|radius_are_invalid',
                message: `'lat' should be in -90...90, 'lon' should be in -180...180, 'radius' should be in 1...${MAX_RADIUS}`,
            });
            return;
        }

        const t = await models.sequelize.transaction();

        if (limit > 100) {
            logger.critical(`ads - limit is over: ${limit}`);
            limit = 100;
        }

        try {
            const foundAds = await models.Advert.findAll({
                order: [['createdAt', 'DESC']],
                include: [{model: models.User, as: 'User'}, models.Category, models.Media],
                limit,
                offset,
                transaction: t,
                where: buildWhere(customWhere, useGeo && {lat, lon, radius}),
            });

            await models.Advert.prepareForApi(req.user, foundAds, t);

            await t.commit();
            res.send({status: 200, ads: foundAds});
        } catch (err) {
            await t.rollback();
            next(err);
        }
    };
};

export const advertGetOneController: RequestHandler = async (req, res, next) => {
    // TODO: think about status
    try {
        const foundAd = await models.Advert.findByPk(req.params.advertId, {
            include: [{model: models.User, as: 'User'}, models.Category, models.Media],
        });

        if (!foundAd) {
            res.status(400).send({status: 400, message: 'not found'});
            return;
        }

        await models.Advert.prepareForApi(req.user, [foundAd]);

        res.send({status: 200, ad: foundAd});
    } catch (err) {
        next(err);
    }
};

export const advertCreateController: RequestHandler = async (req, res, next) => {
    const {title, text, coordinates, categoryId} = req.body;

    if (!title) {
        res.status(400).send({status: 400, desc: 'title_required'});
        return;
    }

    if (!coordinates || coordinates.length !== 2) {
        res.status(400).send({status: 400, desc: 'coordinates_required'});
        return;
    }

    const imagesIds = normalizeImagesIds(req.body.imagesIds),
        newAdvertObject = {
            title,
            text,
            coordinates: {type: 'Point', coordinates: coordinates.reverse()},
        },
        t = await models.sequelize.transaction();

    try {
        const createdAdvert: models.Advert = await req.user.$create('Advert', newAdvertObject, {
                transaction: t,
            }) as models.Advert,
            results = await Promise.all([
                imagesIds.length ? models.Media.findAll({where: {id: {[Op.in]: imagesIds}}, attributes: ['id'], transaction: t}) : null,
                categoryId ? models.Category.findByPk(categoryId, {attributes: ['id'], transaction: t}) : null,
            ] as Array<Promise<models.Media[] | null | models.Category> | null>),
            images: models.Media[] = results[0] as models.Media[],
            category: models.Category = results[1] as models.Category;

        await Promise.all([
            images && createdAdvert.$set('Media', images, {transaction: t}),
            category && createdAdvert.$set('Category', category, {transaction: t}),
        ]);

        await createdAdvert.reload({
            transaction: t,
            include: [{model: models.User, as: 'User'}, models.Category, models.Media],
        });

        createdAdvert.questionsCount = 0;

        await t.commit();
        res.send({status: 200, ad: createdAdvert});
    } catch (err) {
        logger.critical('trace', err);
        await t.rollback();
        next(err);
    }
};

export const advertUpdateController: RequestHandler = async (req, res, next) => {
    const {title, text, coordinates, categoryId, status} = req.body,
        imagesIds = normalizeImagesIds(req.body.imagesIds);

    if (!title) {
        res.status(400).send({status: 400, desc: 'title_required'});
        return;
    }
    if (!imagesIds) {
        res.status(400).send({status: 400, desc: 'imagesIds_required'});
        return;
    }
    if (typeof categoryId === 'undefined') {
        res.status(400).send({status: 400, desc: 'categoryId_required'});
        return;
    }
    if (!coordinates || coordinates.length !== 2) {
        res.status(400).send({status: 400, desc: 'coordinates_required'});
        return;
    }

    const t = await models.sequelize.transaction();

    try {
        const foundAdverts: models.Advert[] = await req.user.$get('Adverts', {where: {id: req.params.advertId}, transaction: t}) as models.Advert[],
            foundAdvert: models.Advert = foundAdverts?.[0],
            advertUpdateObject = {
                title,
                text,
                status: status === 'paused' ? status : 'for_moderate',
                coordinates: {type: 'Point', coordinates: coordinates.reverse()},
            };

        if (!foundAdvert) {
            await t.rollback();
            res.status(404).send({status: 404, message: 'advert is not found'});
            return;
        }

        if (status && status !== 'paused' && foundAdvert.status !== status) {
            await t.rollback();
            res.status(400).send({status: 400, message: 'status can be updated only to `paused`'});
            return;
        }

        await Promise.all([
            foundAdvert.update(advertUpdateObject, {transaction: t}),
            foundAdvert.$set('Media', imagesIds as any, {transaction: t}),
            foundAdvert.$set('Category', categoryId, {transaction: t}),
        ]);
        await foundAdvert.reload({
            transaction: t,
            include: [{model: models.User, as: 'User'}, models.Category, models.Media],
        });

        await models.Advert.prepareForApi(req.user, [foundAdvert], t);

        await t.commit();
        res.send({status: 200, ad: foundAdvert});
    } catch (err) {
        logger.critical('trace', err);
        await t.rollback();
        next(err);
    }
};
