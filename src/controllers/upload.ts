
import * as path from 'path';
import {RequestHandler} from 'express';
import * as fs from 'fs-extra';
import {isEmpty, pick, omit} from 'lodash';
import * as  models from '../models';
import genImagePreviews from '../lib/genImagePreviews';
import pathsHelper from '../pathsHelper';
import _logger from '../lib/logger';

type MainFields = 'path';

const logger = _logger.getLogger('routes/api/upload'),
    uploadFolderPath = pathsHelper.media,
    mediaPreviews = {
        '150x150': {
            width: 150,
            height: 150,
        },
    },
    mainFields: MainFields[] = ['path'];

fs.ensureDir(uploadFolderPath).catch(logger.critical);

interface File {
    url: string;
    path: string;
    previews: any;
    meta: any;
}

export const uploadController: RequestHandler = async (req, res, next) => {
    const responses = [];

    try {
        for (const fileName of Object.keys(req.files)) {
            const file = req.files[fileName],
                userFolder = '/public/storage/media',
                newFilename = file.hash + path.extname(file.name),
                newFilePath = path.join(uploadFolderPath, newFilename),
                userUrl = path.join(userFolder, newFilename);

            try {
                await fs.move(file.path, newFilePath, {overwrite: true});
            } catch (err) {
                await fs.remove(file.path);
                throw err;
            }

            for (const preview of Object.keys(mediaPreviews)) {
                const previewParams = mediaPreviews[preview],
                    previewName = newFilename.replace(
                        file.hash, file.hash + '_preview' + previewParams.width + 'x' + previewParams.height
                    ),
                    options = {
                        previewUrl: path.join(userFolder, previewName),
                        previewPath: path.join(uploadFolderPath, previewName),
                        newFilePath: newFilePath,
                        previewParams: previewParams,
                    },
                    previewsObjects = await genImagePreviews(options),
                    resp: File = {
                        url: userUrl,
                        path: newFilePath,
                        previews: undefined as any,
                        meta: undefined as any,
                    };

                if(!isEmpty(previewsObjects)) {
                    resp.previews = Object.assign({}, resp.previews, previewsObjects);
                }

                responses.push(resp);
            }
        }

        for (const file of responses) {
            const newMedia = pick<File, MainFields | 'meta'>(file, ['path', 'meta']);

            newMedia.meta = omit(file, mainFields);

            const createdMedia = await models.Media.create(newMedia);

            res.send({status: 200, media: createdMedia});
        }
    } catch (err) {
        next(err);
    }
};
