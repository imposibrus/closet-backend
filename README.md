# closet-backend
## Directory structure:
```
|
|- src/                   - sources folder
|---- bin/
|------- www              - entry point. main executable
|---- lib/                - common libraries
|---- models/             - DB models
|---- routes/             - site routes
|---- controllers/        - site controllers
|- node_modules/          - third-party back-end modules
|- public/                - site public files
|---- storage/            - uploads - user(or admin)-generated content
|---- swagger-ui/         - self-hosted swagger-ui
|- test/                  - tests
|- views/                 - site templates
```

## [Swagger API documentation](http://closet.imposibrus.tk/public/swagger-ui/)
